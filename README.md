**AddPack, AddItem, ListItems**: **Organize and complete these and be done with it!!**
  
  
  
**Current current task**  
+ Update AddItem so that photo taking is not done here  
+ The database stuff for the photo could go in its own activity  
+ Add a photo from ItemDetail/PackDetail  
+ Add a photo also from ListItem, the context menu when long-clicking an item  
  + Make a new context menu action  
+ Remove a photo with AddItem/AddPack in Edit mode  
  + Remove a photo in ItemDetail also?  

**Current tasks**  
  
+ NewsFeedActivity for main activity  
  
+ Fix item photo numbering  
  
+ Context menus on long-click in ListItems, ListPacks, NewsFeed  
  
+ Update database EditItem, EditPack, ItemsInPack  
  
+ Activity to add an Item to a Pack (implement and test selectable mode)  

+ Organize and group methods by subject in ListItems, AddItem, ItemDetail, ListPacks, AddPack, PackDetail, DB classes, other classes  
  
+ Complete documentation for all classes    
  
  
**Next big thing on the agenda**  
+ User accounts  
  

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions