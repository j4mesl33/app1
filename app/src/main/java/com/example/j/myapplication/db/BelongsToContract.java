package com.example.j.myapplication.db;

import android.provider.BaseColumns;

public final class BelongsToContract {

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor
    public BelongsToContract() {}

    public static abstract class BelongsToEntry implements BaseColumns {
        // Inherited fields _ID and _COUNT
        public static final String TABLE_NAME = "item_belongs_to";
        public static final String COLUMN_NAME_PACK_ID = "pack_id";
        public static final String COLUMN_NAME_ITEM_ID = "item_id";
    }

}

