package com.example.j.myapplication;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Utility {

    /**
     * Show a toast.
     * @param context The application context
     * @param s The string to display
     */
    public static void toast(Context context, String s) {
        Toast toast = Toast.makeText(context, s, Toast.LENGTH_LONG);
        toast.show();
    }

    /**
     * Takes a ViewGroup representing an input form and clears/resets its child views and
     * grandchild views. Clears <code>EditText</code>s and <code>CheckBox</code>es, and resets
     * <code>Spinner</code>s and <code>NumberPicker</code>s.<br><br>
     *
     * Example use:<br>
     * <code>Utility.clearFields((ViewGroup) findViewById(R.id.viewgroup_add_item));</code>
     *
     * @param group The ViewGroup whose children should be cleared/reset
     */
    public static void clearFields(ViewGroup group) {
        for (int i = 0, count = group.getChildCount(); i < count; ++i) {
            View childView = group.getChildAt(i);

            if (childView instanceof LinearLayout) {
                // Clear grandchild views
                ViewGroup subgroup = (ViewGroup) childView;
                for (int j = 0, subgroupCount = subgroup.getChildCount(); j < subgroupCount; ++j) {
                    View grandchildView = subgroup.getChildAt(j);
                    clearField(grandchildView);
                }
            } else {
                // Clear child views
                clearField(childView);
            }
        }
    }

    private static void clearField(View view) {
        if (view instanceof EditText) {
            ((EditText) view).setText("");
        } else if (view instanceof Spinner) {
            ((Spinner) view).setSelection(0, true);
        } else if (view instanceof NumberPicker) {
            ((NumberPicker) view).setValue(0);
        } else if (view instanceof CheckBox) {
            ((CheckBox) view).setChecked(false);
        }
    }

    public static void removeChildTextViews(ViewGroup group) {
        for (int i = 0; i < group.getChildCount(); i++) {
            View childView = group.getChildAt(i);
//            if (childView instanceof TextView) {
            if (childView.getClass().equals(TextView.class)) {
                group.removeView(childView);
            }
        }
    }

    /**
     * Takes a String for the amount and a String for the units. If the amount is singular, i.e. "1",
     * and the unit is "pounds", return "1 pound" with the "s" removed, to avoid awkwardly displaying "1 pounds".
     * TODO Possibly expensive? To use this in a List activity, this static method will be called for each row
     * Better to return a new String or modify the 'units' String?
     * @param amount The String amount
     * @param units The type of unit used for the amount
     */
    public static String trimUnitsWhenSingular(String amount, String units) {
        if (amount.equals("1")) {
            return amount + " " + units.substring(0, units.length() - 1);
        } else {
            return amount + " " + units;
        }
    }

    public static String trimUnitsWhenSingular(int amount, String units) {
        if (amount == 1) {
            return amount + " " + units.substring(0, units.length() - 1);
        } else {
            return amount + " " + units;
        }
    }
}
