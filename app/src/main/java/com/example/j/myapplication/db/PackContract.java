package com.example.j.myapplication.db;

import android.provider.BaseColumns;

public final class PackContract {

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor
    public PackContract() {}

    public static abstract class PackEntry implements BaseColumns {
        // Inherited fields _ID and _COUNT
        public static final String TABLE_NAME = "pack";
        public static final String COLUMN_NAME_PACK_NAME = "name";
        public static final String COLUMN_NAME_CAPACITY = "capacity";
        public static final String COLUMN_NAME_CAPACITY_UNITS = "capacity_units";
        public static final String COLUMN_NAME_CAPACITY_IN_CUBIC_INCHES = "capacity_in_cubic_inches";
        public static final String COLUMN_NAME_NOTES = "notes";
    }

}
