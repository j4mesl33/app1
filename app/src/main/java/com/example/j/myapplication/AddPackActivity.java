package com.example.j.myapplication;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;

import com.example.j.myapplication.db.PackContract;
import com.example.j.myapplication.db.PackDbHelper;

/*
  Completely organized:                         12/08/14
  Everything in this class last accounted for:  11/18/14

 - TODO: 'Uses' tags, i.e. cycling, hiking; 'Roles'; 'Survival essentials fulfilled'
   - Have an EditText (single line) with an "Add" button next to it
   - The user types in a word or phrase to use as a tag and presses "Add"
   - Upon pressing "Add", the tag appears below the input field
   - Display the tag balloon, with the tag text (automatically lowercase)
    and an "X" to close

 - TODO: Dropdown display for 'Survival essentials'
   - i.e. 7/10
   - Click to display line-by-line, essential => tool

 - TODO: When long-pressing a ListView item, the ContextMenu displays for the first list item only,
         and not for the correct item

*/


/**
  The activity that allows a user to create a new Pack. A Pack represents an instance
  of a backpack or other type of container for holding Items. A Pack holds zero or more Items.

                                                <br><br>
    Constants                                   {@link #TAG}<br>
    Member variables                            {@link #editMode}
    UI elements                                 {@link #editPackName}<br><br>

    Android activity lifecycle methods          {@link #onCreate(Bundle)}<br>
    Initialization methods                      {@link #getUiHooks()}<br>
    Methods that respond to user clicks         {@link #submit(View)}<br>
    Methods that read from the database         {@link #getExistingPack(long)}<br>
    Methods that manage the options menu        {@link #onCreateOptionsMenu(Menu)}<br><br>

  @author James
 */
public class AddPackActivity extends Activity {

    /* Constants */

    /**
     * Android logging tag
     */
    private static final String TAG = "AddPackActivity";

    /**
     * Intent extra key. If this key is received, this activity will edit an existing Pack instead
     * of creating a new one from initially blank fields. Used to pass the row ID of the Pack that
     * should be displayed.
     */
    public static final String ROW_ID_KEY = "RowID";

    /**
     * The value for the row ID if a value was not received as an Intent extra
     */
    private static final long ROW_ID_NOT_RECEIVED = -1;

    /**
     * Intent extra key. If this key is received, this activity will set the focus to the View that
     * has the <code>int</code> ID given.
     */
    public static final String VIEW_FOCUS_KEY = "ViewFocus";

    /**
     * The maximum value allowable in the capacity number picker.
     * Based on how an 80-liter pack is equivalent to about 5,000 cubic inches.
     */
    private static final int NUMBER_PICKER_MAX_VALUE = 10000;



    /* Member variables */

    /**
     * Is set to true if the activity should edit an existing Pack instead of creating a new Pack,
     * false if it should create a new Pack, as is normal
     */
    private boolean editMode;

    /**
     * The row ID of the Pack in the database to display
     */
    private long rowID;

    /**
     * The database helper for the Pack table
     */
    private PackDbHelper packDbHelper;



    /* UI elements */

    /**
     * Name
     */
    private EditText editPackName;

    /**
     * Capacity
     */
    private NumberPicker editPackCapacity;

    /**
     * Capacity units
     */
    private Spinner editPackCapacityUnits;

    /**
     * Notes
     */
    private EditText editPackNotes;



    /* Android activity lifecycle methods */

    /**
     * Called when the activity is starting
     * @param savedInstanceState If the activity is being re-initialized after previously being shut
     *                           down, then this Bundle contains the data it most recently supplied
     *                           in onSaveInstanceState(Bundle). Otherwise it is null.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pack);

        // Find the input fields, and log error messages if any necessary Views are not found
        getUiHooks();

        // Prepare the database helper
        packDbHelper = new PackDbHelper(getApplicationContext());

        // Check the Intent extras to see if the activity should edit an existing Pack
        // instead of creating a new Pack
        checkEditMode();

        Log.i(TAG, "Activity created");
    }



    /* Initialization methods */

    /**
     * Set references to the UI elements, and log error messages if any necessary Views
     * are not found
     */
    private void getUiHooks() {
        // Get the UI hooks
        editPackName = (EditText) findViewById(R.id.edit_pack_name);
        editPackCapacity = (NumberPicker) findViewById(R.id.edit_pack_capacity);
        editPackCapacityUnits = (Spinner) findViewById(R.id.edit_pack_capacity_units);
        editPackNotes = (EditText) findViewById(R.id.edit_pack_notes);

        // Set capacity units spinner
        ArrayAdapter<CharSequence> unitsAdapter = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.capacity_units_array, android.R.layout.simple_spinner_item);
        unitsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        editPackCapacityUnits.setAdapter(unitsAdapter);

        // Set number picker params
        editPackCapacity.setMinValue(0);
        editPackCapacity.setMaxValue(NUMBER_PICKER_MAX_VALUE);
        editPackCapacity.setWrapSelectorWheel(false);

        // Log error messages if any necessary Views are not found
        if (editPackName == null) {
            Log.e(TAG, "EditText 'edit_pack_name' not found!");
        } if (editPackCapacity == null) {
            Log.e(TAG, "NumberPicker 'edit_pack_capacity' not found!");
        } if (editPackCapacityUnits == null) {
            Log.e(TAG, "Spinner 'edit_pack_capacity_units' not found!");
        } if (editPackNotes == null) {
            Log.e(TAG, "EditText 'edit_pack_notes' not found!");
        }
    }

    /**
     * Check the Intent extras to see if this activity should edit an existing Pack
     * or create a new Pack
     */
    private void checkEditMode() {
        // Set edit mode to false by default, and the row ID to -1, to indicate that
        // a row ID was not received
        editMode = false;
        rowID = ROW_ID_NOT_RECEIVED;

        // Read the Pack ID from the Intent extra if one was received
        Intent intent = getIntent();
        if (intent.hasExtra(ROW_ID_KEY)) {
            editMode = true;
            rowID = intent.getLongExtra(ROW_ID_KEY, ROW_ID_NOT_RECEIVED);
        }

        // Log the activity's mode and set the appropriate activity title
        if (!editMode) {
            Log.i(TAG, "AddPack will run in 'normal' mode and create a new Pack");
        } else {
            Log.i(TAG, "AddPack will run in 'edit' mode and edit an existing Pack, row ID " + rowID);

            String myPackName = getExistingPackName(rowID, packDbHelper.getReadableDatabase());
            getActionBar().setTitle("Edit " + myPackName);
        }

        // If requested, set the focus on the specified focusable View
        boolean shouldFocus = intent.hasExtra(VIEW_FOCUS_KEY);
        if (shouldFocus) {
            int viewID = intent.getIntExtra(VIEW_FOCUS_KEY, 0);
            setFocus(viewID);
        } else if (editMode) {
            // If the activity should edit an existing item,
            // but focus was not requested on a specific view
            Log.i(TAG, "Focus was not requested on a specific view");
        }

        // Finally, when running in edit mode, fill in the input fields with the existing Pack's
        // values, so that they can be edited
        if (editMode) {
            fillInputFields();
        }
    }

    /**
     * Set focus on the specified View
     * @param viewID The ID of the View on which to focus
     */
    private void setFocus(int viewID) {
        Log.i(TAG, "Focus was requested on View ID " + viewID);
        View v = findViewById(viewID);
        v.requestFocus();
    }

    /**
     * Fill in the input fields with the existing Pack's values if the activity is in edit mode
     */
    private void fillInputFields() {
        Pack existingPack = getExistingPack(rowID);

        editPackName.setText(existingPack.getName());
        editPackCapacity.setValue(existingPack.getCapacity());

//        editPackCapacityUnits.
        // TODO
        // Need to set the spinner value: May need to read the String value from the object,
        // find the Spinner position that uses the same String, then set the value using that position index.
        //
        // Is there a more elegant way to do this?
        //   - Store the Spinner index perhaps? (The concern is that
        //     the array might need to be changed in the future, changing the indices)
        //   - Enum class for capacity units?

        editPackNotes.setText(existingPack.getNotes());
    }



    /* Methods that respond to user clicks */

    /**
     * Respond when the user presses the 'Submit' button.
     * Create the new Pack in the database if running in normal mode,
     * and update the Pack in the database if running in edit mode.
     * @param v The View that was clicked
     */
    public void submit(View v) {
        // Read the input fields
        String packName = editPackName.getText().toString();
	    int packCapacity = editPackCapacity.getValue();
	    String packCapacityUnits = editPackCapacityUnits.getSelectedItem().toString();
	    String packNotes = editPackNotes.getText().toString();

        // Construct a new Pack object as read from the current input fields
        Pack newPack = new Pack(packName, packCapacity, packCapacityUnits, packNotes);

        /* Normal mode: Add a new Pack */
        if (!editMode) {
            // Log the new Pack's toString
	        Log.i(TAG, "Pack created:\n" + newPack.toString());

            // Write the new Pack to the Pack database and get its row ID
	        long rowID = addPackToDatabase(newPack);
            // The row ID was already logged in addPackToDatabase()

            // Close the database helper and finish the activity
            packDbHelper.close();
	        this.finish();
        }
        /* Edit mode: Update an existing Pack */
        else {
            // Log the updated Pack's toString
	        Log.i(TAG, "Pack updated:\n" + newPack.toString());

            // Update the Pack that already exists in the database
            updatePackInDatabase(newPack);

            // Close the database helper and finish the activity
            packDbHelper.close();
            this.finish();
        }
    }

    /**
     * Clear all of the input fields in the activity's UI
     */
    public void clearFields(View v) {
        Utility.clearFields((ViewGroup) findViewById(R.id.viewgroup_add_pack));
    }



    /* Methods that read from the database */

    /**
     * Return the Pack that has the row ID <code>myRowID</code>,
     * or null if no Pack exists with that row ID
     * @param myRowID The row ID of the Pack to fetch
     * @return The Pack that has the row ID <code>myRowID</code>,
     *         or null if no Pack exists with that row ID
     */
    private Pack getExistingPack(long myRowID) {
        // Decided to use the method argument 'myRowID' instead of the member variable 'rowID',
        // so that this method can be easily moved to a different class.
        // Opted not to make this method static (for now) because it uses the application Context
        // (for the database helper)

        // Access the database
        SQLiteDatabase db = packDbHelper.getReadableDatabase();

        // Select by the row ID
        String[] selectionArg = { String.valueOf(myRowID) };

        // Query the database and get a cursor
        Cursor c = db.query(
                PackContract.PackEntry.TABLE_NAME,
                PackDetailActivity.PROJECTION,
                PackDetailActivity.ROW_ID_SELECTION,	// Select by the row ID.
                selectionArg,	// Pass the row ID as the selection argument.
                null,			// Do not group the rows in the result.
                null,			// Set to null because 'groupBy' is also set to null.
                null);			// Do not sort the result, only 1 row is expected.

        Log.i(TAG, c.getCount() + " row(s) in the cursor. 1 row expected.");

        // Return null if a row was not found with the given row ID
        if (c.getCount() == 0) {
            packDbHelper.close();
            Log.d(TAG, "No row found with row ID " + myRowID);
            return null;
        } else if (c.getCount() != 1) {
            Log.d(TAG, "Query for row ID " + myRowID + " was unsuccessful");
        }

        // Read the fields from the cursor
        c.moveToFirst();
        String existingPackName =
                c.getString(c.getColumnIndexOrThrow(PackContract.PackEntry.COLUMN_NAME_PACK_NAME));
        int existingCapacity =
                c.getInt(c.getColumnIndexOrThrow(PackContract.PackEntry.COLUMN_NAME_CAPACITY));
        String existingCapacityUnits =
                c.getString(c.getColumnIndexOrThrow(PackContract.PackEntry.COLUMN_NAME_CAPACITY_UNITS));
        String existingNotes =
                c.getString(c.getColumnIndexOrThrow(PackContract.PackEntry.COLUMN_NAME_NOTES));

        // Create and return the Pack
        Pack existingPack = new Pack(existingPackName, existingCapacity, existingCapacityUnits, existingNotes);
        return existingPack;
    }

    /**
     * Given the row ID, return the name of the Pack that is currently stored in the database
     * or null if no such Pack exists
     * @param myRowID The row ID of the Pack to fetch
     * @return The name of the Pack that has the row ID <code>rowID</code>, or null if no such Pack exists
     * TODO Projection with just the name
     */
    private static String getExistingPackName(long myRowID, SQLiteDatabase db) {
        // Select by the row ID
        String[] selectionArg = { String.valueOf(myRowID) };

        // Query the database and get a cursor
        Cursor c = db.query(
                PackContract.PackEntry.TABLE_NAME,
                PackDetailActivity.PROJECTION,
                PackDetailActivity.ROW_ID_SELECTION,    // Select by the row ID.
                selectionArg,    // Pass the row ID as the selection argument.
                null,            // Do not group the rows in the result.
                null,            // Set to null because 'groupBy' is also set to null.
                null);			// Do not sort the result, only 1 row is expected.

        Log.i(TAG, "Querying for the name of the Pack with row ID " + myRowID + "...");
        Log.i(TAG, c.getCount() + " row(s) in the cursor. 1 row expected.");

        // Return null if a row was not found with the given row ID
        if (c.getCount() == 0) {
            Log.d(TAG, "No row found with row ID " + myRowID);
            return null;
        } else if (c.getCount() != 1) {
            Log.d(TAG, "Query for row ID " + myRowID + " was unsuccessful");
            return null;
        }

        // Read the field from the cursor
        c.moveToFirst();
        String existingPackName =
                c.getString(c.getColumnIndexOrThrow(PackContract.PackEntry.COLUMN_NAME_PACK_NAME));
        Log.i(TAG, "The Pack with row ID " + myRowID + " is named '" + existingPackName + "'");
        return existingPackName;
    }



    /* Methods that write to the database */

    /**
     * Add a completely new Pack to the database
     * @return <code>long</code> row ID for the newly created Pack's row ID in the database,
     * or <code>-1</code> if the database insert was not successful
     */
    private long addPackToDatabase(Pack pack) {
        // Get the database in write mode
        SQLiteDatabase db = packDbHelper.getWritableDatabase();

        // Create a map of values, with column names as the keys and data attributes as the values
        ContentValues values = new ContentValues();
        values.put(PackContract.PackEntry.COLUMN_NAME_PACK_NAME, pack.getName());
        values.put(PackContract.PackEntry.COLUMN_NAME_CAPACITY, pack.getCapacity());
        values.put(PackContract.PackEntry.COLUMN_NAME_CAPACITY_UNITS, pack.getCapacityUnits());
        values.put(PackContract.PackEntry.COLUMN_NAME_CAPACITY_IN_CUBIC_INCHES, pack.getCapacityInCubicInches());
        values.put(PackContract.PackEntry.COLUMN_NAME_NOTES, pack.getNotes());

        // Insert the new row
        long newRowId = db.insert(PackContract.PackEntry.TABLE_NAME, null, values);

        if (newRowId == -1) {
            /* The database insert was unsuccessful */
            Log.e(TAG, "New Pack database insert was unsuccessful!");
        } else {
            /* The database insert was successful */
            Log.e(TAG, "New Pack row ID, " + newRowId);
        }
        return newRowId;
    }

    /**
     * Update an existing Pack in the database
     * @param newPack The new Pack to write to the database, overwriting the existing Pack that has
     *                the row ID <code>rowID</code>
     * @return True if the update was successful, false if it was unsuccessful
     */
    private boolean updatePackInDatabase(Pack newPack) {
        // Get the database in write mode
        SQLiteDatabase db = packDbHelper.getWritableDatabase();

        // Get the Pack that currently exists in the database so that it can be compared against
        // the new Pack, as read from the input fields
        Pack existingPack = getExistingPack(rowID);

        // Create a map of values, with column names as the keys and data attributes as the values
        ContentValues values = new ContentValues();
        if (!existingPack.getName().equals(newPack.getName())) {
            values.put(PackContract.PackEntry.COLUMN_NAME_PACK_NAME, newPack.getName());
        } if (existingPack.getCapacity() != newPack.getCapacity()) {
            values.put(PackContract.PackEntry.COLUMN_NAME_CAPACITY, newPack.getCapacity());
        } if (!existingPack.getCapacityUnits().equals(newPack.getCapacityUnits())) {
            values.put(PackContract.PackEntry.COLUMN_NAME_CAPACITY_UNITS, newPack.getCapacityUnits());
        } if (existingPack.getCapacityInCubicInches() != newPack.getCapacityInCubicInches()) {
            values.put(PackContract.PackEntry.COLUMN_NAME_CAPACITY_IN_CUBIC_INCHES, newPack.getCapacityInCubicInches());
        } if (!existingPack.getNotes().equals(newPack.getNotes())) {
            values.put(PackContract.PackEntry.COLUMN_NAME_NOTES, newPack.getNotes());
        }

        // Select by the row ID
        String[] selectionArg = { String.valueOf(rowID) };

        // Insert the new row
        int numRowsAffected = db.update(PackContract.PackEntry.TABLE_NAME,
                                        values,
                                        PackDetailActivity.ROW_ID_SELECTION,
                                        selectionArg);

        if (numRowsAffected == 1) {
            /* The database update was successful */
            Log.i(TAG, "Pack updated in database, row ID " + rowID);
            return true;
        } else if (numRowsAffected == 0) {
            /* The database update was unsuccessful */
            Log.e(TAG, "Pack update for row ID " + rowID + " was unsuccessful!");
            return false;
        } else {
            // The number of rows affected was not zero or one,
            // which means that db.update() returned a number greater than one,
            // or a negative number. This is cause for concern!
            // Multiple rows may exist in the database that have the same row ID,
            // or db.update() returned a completely unexpected and unspecified result.
            Log.wtf(TAG, "Pack update returned an unexpected result: " + numRowsAffected + "!");
            return false;
        }
    }



    /* Methods that manage the options menu */

    /**
     * Initialize the activity's option menu
     * @param menu The options menu in which to place items
     * @return True if the menu is to be displayed, false otherwise
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_pack, menu);
        return true;
    }

    /**
     * Called whenever an item in the options menu is selected
     * @param item The menu item that was selected
     * @return Return false to allow normal menu processing to proceed, true to consume it here
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        /* TODO
        Put 'Done' and 'Cancel' buttons in the Action Bar? It seems like it may be
        the standard, as opposed to submit buttons at the bottom of the activity.
        */

        return super.onOptionsItemSelected(item);
    }

}
