package com.example.j.myapplication;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.j.myapplication.db.ItemContract;

public class ItemAdapter extends CursorAdapter {

    Context context;
    LayoutInflater inflater;

    /**
     * Recommended constructor.
     *
     * @param context The context
     * @param c       The cursor from which to get the data.
     * @param flags   Flags used to determine the behavior of the adapter; may
     *                be any combination of {@link #FLAG_AUTO_REQUERY} and
     *                {@link #FLAG_REGISTER_CONTENT_OBSERVER}.
     */
    public ItemAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Makes a new view to hold the data pointed to by cursor.
     *
     * @param context Interface to application's global information
     * @param cursor  The cursor from which to get the data. The cursor is already
     *                moved to the correct position.
     * @param parent  The parent to which the new view is attached to
     * @return the newly created view.
     */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(R.layout.custom_row_item, parent, false); // TODO Try true/false
    }

    /**
     * Bind an existing view to the data pointed to by cursor
     *
     * @param view    Existing view, returned earlier by newView
     * @param context Interface to application's global information
     * @param cursor  The cursor from which to get the data. The cursor is already
     */
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView itemName = (TextView) view.findViewById(R.id.custom_row_item_name);
        TextView itemCategory = (TextView) view.findViewById(R.id.custom_row_item_category);
        TextView itemWeight = (TextView) view.findViewById(R.id.custom_row_item_weight);
        TextView itemQuantity = (TextView) view.findViewById(R.id.custom_row_item_quantity);

        itemName.setText(cursor.getString(cursor.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_ITEM_NAME)));
        itemCategory.setText("[" + cursor.getString(cursor.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_CATEGORY)) + "]");

        // Call trimUnitsWhenSingular, which will trim awkward instances of "1 pounds" to "1 pound"
        itemWeight.setText(Utility.trimUnitsWhenSingular(
                cursor.getString(cursor.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT)),
                cursor.getString(cursor.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_UNITS))));

        itemQuantity.setText("Quantity: " + cursor.getString(cursor.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_QUANTITY)));
    }
}
