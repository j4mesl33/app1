package com.example.j.myapplication.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * The database helper for the Item class
 * @author James
 */
public class ItemDbHelper extends SQLiteOpenHelper {

    /**
     * The database version, to be incremented with each database version
     */
    public static final int DATABASE_VERSION = 5;

    /**
     * The database name
     */
    public static final String DATABASE_NAME = "Item.db";

    /**
     * SQL statement that creates and deletes the Item table
     */
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + ItemContract.ItemEntry.TABLE_NAME
                    + " (" + ItemContract.ItemEntry._ID + " INTEGER PRIMARY KEY,"
                    + ItemContract.ItemEntry.COLUMN_NAME_ITEM_NAME
                    + Db.TEXT_TYPE + Db.COMMA
                    + ItemContract.ItemEntry.COLUMN_NAME_CATEGORY
                    + Db.TEXT_TYPE + Db.COMMA
                    + ItemContract.ItemEntry.COLUMN_NAME_WEIGHT
                    + Db.INTEGER_TYPE + Db.COMMA
                    + ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_UNITS
                    + Db.TEXT_TYPE + Db.COMMA
                    + ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_IN_GRAMS
                    + Db.INTEGER_TYPE + Db.COMMA
                    + ItemContract.ItemEntry.COLUMN_NAME_QUANTITY
                    + Db.INTEGER_TYPE + Db.COMMA
                    + ItemContract.ItemEntry.COLUMN_NAME_IS_ESSENTIAL
                    + Db.BOOLEAN_TYPE + Db.COMMA
                    + ItemContract.ItemEntry.COLUMN_NAME_NOTES
                    + Db.TEXT_TYPE + Db.COMMA
                    + ItemContract.ItemEntry.COLUMN_NAME_PHOTO_FILE_PATH
                    + Db.TEXT_TYPE + " )";

    /**
     * SQL statement to delete the Item table
     */
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + ItemContract.ItemEntry.TABLE_NAME;

    /**
     * Construct the Item database helper
     * @param context The application context
     */
    public ItemDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     * @param db The database
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p/>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database
     * @param oldVersion The old database version
     * @param newVersion The new database version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    /**
     * Called when the database needs to be downgraded
     * @param db The database
     * @param oldVersion The old database version
     * @param newVersion The new database version
     */
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
