package com.example.j.myapplication;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.example.j.myapplication.db.PackContract;
import com.example.j.myapplication.db.PackDbHelper;

/*

  Methods organized and completely documented: 12/07/14
  Everything in this class last accounted for: 11/15/14

  TODO:
  - Settings
  - Confirmation for 'Delete all'
  - Switch statement for Loader (low priority)

 */

/**
  An activity that displays a list of the Packs stored in the database.
  Allows the user to launch <code>AddPackActivity</code> to create and save a new Pack,
  or click a list item to view it in more detail with <code>PackDetailActivity</code>.

                                            <br><br>
    Constants:                              {@link #TAG}<br>
    Member variables:                       {@link #packDbHelper}<br>
    UI elements:                            {@link #layout}<br><br>

    Android lifecycle methods:              {@link #onCreate(Bundle)}<br>
    Initialization methods:                 {@link #getUiHooks()}<br>
    Methods that respond to user clicks:    {@link #addPack(View)}<br>
    Methods that manage the cursor loader:  {@link #refreshCursor()}<br>
    Methods that write to the database:     {@link #deleteAllDatabaseItems()}<br>
    Methods that manage the context menu:   {@link #onCreateContextMenu(ContextMenu, View, ContextMenu.ContextMenuInfo)}<br>
    Methods that manage the options menu:   {@link #onCreateOptionsMenu(Menu)}<br>
    Other methods:                          {@link #showProgressBar()}<br><br>

  @author James
 */
public class ListPacksActivity extends ListActivity
        implements LoaderManager.LoaderCallbacks<Cursor> { /** For the database cursor */

    /* Constants */

    /**
     * Android logging tag
     */
    private static final String TAG = "ListPacksActivity";

    /**
     * The columns to select from the database table
     */
    private static final String[] PROJECTION = {
            PackContract.PackEntry._ID,
            PackContract.PackEntry.COLUMN_NAME_PACK_NAME,
            PackContract.PackEntry.COLUMN_NAME_CAPACITY,
            PackContract.PackEntry.COLUMN_NAME_CAPACITY_UNITS,
            PackContract.PackEntry.COLUMN_NAME_CAPACITY_IN_CUBIC_INCHES,
            PackContract.PackEntry.COLUMN_NAME_NOTES };

    /**
     * The default sort order for the Pack database query results
     */
    private static final String DEFAULT_SORT_ORDER = PackContract.PackEntry.COLUMN_NAME_PACK_NAME;

    /**
     * Identifies the loader used to load the Packs database cursor
     */
    private static final int PACKS_LOADER = 0;



    /* Member variables */

    /**
     * The database helper for the Pack database
     */
    private PackDbHelper packDbHelper;



    /* UI elements */

    /**
     * The view group that stores this activity's views
     */
    private RelativeLayout layout;

    /**
     * The ListView that stores the database items
     */
    private ListView listView;

    /**
     * The adapter used to display the list's data
     */
    private CursorAdapter mAdapter;

    /**
     * The progress bar that displays while loading from the database
     */
    private ProgressBar progressBar;



    /* Android lifecycle methods */

    /**
     * Called when the activity is starting
     * @param savedInstanceState If the activity is being re-initialized after previously being shut
     *                           down, then this Bundle contains the data it most recently supplied
     *                           in onSaveInstanceState(Bundle). Otherwise it is null.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_packs);

        // Access the Packs database table
        packDbHelper = new PackDbHelper(getApplicationContext());

        // Get UI hooks, and log error messages if any necessary Views are not found
        getUiHooks();

        // Register the list view for the context menu
        registerForContextMenu(listView);

        // Set the progress bar, already defined in the XML,
        // to display while the list loads
        progressBar = (ProgressBar) findViewById(R.id.progress_packs);
        showProgressBar();

        // Set the list view's cursor adapter
        mAdapter = new PackAdapter(getApplicationContext(), null, 0);
        setListAdapter(mAdapter);

        // Prepare the loader.
        // Either re-connect with an existing one, or start a new one.
        getLoaderManager().initLoader(PACKS_LOADER, null, this);

        Log.i(TAG, "Activity created");
    }

    /**
     * Called when the activity is being resumed
     */
    @Override
    protected void onResume() {
        // This lifecycle method is called:
        // - when this activity is first created,
        // - when this activity returns to the foreground (partially obscured), and
        // - when this activity is opened again after having been not-visible (fully obscured)

        // Need to make sure that refreshing the cursor in this method is not redundant,
        // i.e. when first started, onCreate() will read from the database, and this method will
        // then refresh a just-loaded cursor. Use logging to figure out exactly what calls are made,
        // and when.

        // Could pass an activity result back to this activity after creating a new Pack,
        // so that this activity knows to refresh.
        // (With this approach, the cursor would not be refreshed if the user started to create a
        // new Pack but then cancelled. However, in the future, when server functionality is added,
        // and when multiple users may be making updates, it might be nice to simply always refresh
        // on resume because another user may have added an item in the time that the first user
        // spent starting to create an item and then cancelling.)
        super.onResume();

        refreshCursor();
    }

    /**
     * Callback method that responds when a called activity returns with a result
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // If it is necessary for this activity to know the row ID
        // of the newly created Pack, or its fields, accept it here

        /*
        TODO Think of what functionality to add here:
        - Confirmation message when new Pack created?
        - Message when Item added to Pack?
        - Message when zero Packs?
         */

        /*
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

            }
        }
         */
    }



    /* Initialization methods */

    /**
     * Set references to the UI elements
     */
    private void getUiHooks() {
        // Get UI hooks for the view group layout and for the list view
        layout = (RelativeLayout) findViewById(R.id.list_of_packs);
        listView = getListView();

        // Log error messages if any necessary Views are not found
        if (layout == null) {
            Log.e(TAG, "RelativeLayout 'list_of_packs' not found!");
        } if (listView == null) {
            Log.e(TAG, "ListView not found!");
        }
    }



    /* Methods that respond to user clicks */

    /**
     * Respond to the "Add pack" button press and start <code>AddPackActivity</code>
     * @param view The view that was clicked
     */
    public void addPack(View view) {
        // Start AddPackActivity.
        // To receive the new row ID, request it here with startActivityForResult()
        Intent intent = new Intent(this, AddPackActivity.class);
        startActivity(intent);
    }

    /**
     * Respond when a list item is clicked
     * @param l        The ListView
     * @param v        The view that was clicked
     * @param position The position of the item that was clicked
     * @param id       The ID of the item that was clicked
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // Access the data associated with the selected item
        Cursor cursor = (Cursor) (l.getAdapter().getItem(position));

        // Log the clicked item's information
        Log.i(TAG, "Row ID " + id + ", position " + position + " was clicked. Pack: " + cursor.getString(1));

        goToListItemDetailPage(id);
    }

    /**
     * Given the row ID of a Pack, open PackDetailActivity to view detailed
     * information about that Pack
     * @param rowID The row ID of the Pack to display
     */
    private void goToListItemDetailPage(long rowID) {
        // Take the row ID of the clicked Pack and open a detail view page for that Pack
        Intent intent = new Intent(this, PackDetailActivity.class);
        intent.putExtra(PackDetailActivity.ROW_ID_KEY, rowID);
        startActivity(intent);
    }




    /* Methods that manage the cursor loader */

    /**
     * Restart the Packs cursor loader
     */
    private void refreshCursor() {
        getLoaderManager().restartLoader(PACKS_LOADER, null, this);
        Log.i(TAG, "The database cursor was refreshed");
    }

    /**
     * Instantiate and return a new Loader for the given ID
     * @param loaderID The ID of the loader to be created
     * @param bundle Any arguments supplied by the caller
     * @return A new Loader instance that is ready to start loading
     */
    @Override
    public Loader<Cursor> onCreateLoader(int loaderID, Bundle bundle) {

        return new CursorLoader(this,   // Context
                null,                   // Uri, ok if null
                PROJECTION,             // String[] projection
                null,                   // String selection
                null,                   // String[] selectionArgs
                DEFAULT_SORT_ORDER)     // String sortOrder
        {
            /**
             * Called on a worker thread to perform the actual load
             * and to return the result of the load operation
             * @return The result of the load operation
             */
            @Override
            public Cursor loadInBackground() {
                // Display the progress bar animation by calling
                // the showProgressBar method from the UI thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showProgressBar();
                    }
                });

                // Query the Pack database for all of the Packs (all rows),
                // with all columns
                SQLiteDatabase db = packDbHelper.getReadableDatabase();
                return db.query(
                        PackContract.PackEntry.TABLE_NAME,
                        PROJECTION,
                        null,                   // Return all rows in the table.
                        null,                   // Set to null because 'selection' is also set to null
                        null,                   // Do not group the rows in the result.
                        null,                   // Set to null because 'groupBy' is also set to null.
                        DEFAULT_SORT_ORDER);    // Use the default sort order.
            }
        };

        /**
         * TODO
         * Modify this method to use a switch statement with the loader ID
         * https://developer.android.com/training/load-data-background/setup-loader.html#DefineLaunch
         */
    }

    /**
     * Called when a previously created loader has finished loading
     * @param cursorLoader Class that asynchronously loads the cursor
     * @param cursor       Cursor that provides access to the result set returned by a database query
     */
    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        // Swap the new cursor in.
        // (The framework will take care of closing the old cursor once we return)
        mAdapter.swapCursor(cursor);

        // Hide the progress bar animation
        hideProgressBar();
    }

    /**
     * Called when a previously created loader is reset, making the data unavailable
     * @param cursorLoader Class that asynchronously loads the cursor
     */
    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        // This is called when the last Cursor provided to onLoadFinished()
        // above is about to be closed.  We need to make sure we are no
        // longer using it.
        mAdapter.swapCursor(null);
    }



    /* Methods that write to the database */

    /**
     * Delete all Packs from the Packs database
     */
    private void deleteAllDatabaseItems() {
        // Log the method's intent
        Log.i(TAG, "Deleting all Packs from the Packs database...");

        // Access and then delete all rows from the database
        SQLiteDatabase db = packDbHelper.getReadableDatabase();
        int numRowsDeleted = db.delete(PackContract.PackEntry.TABLE_NAME, "1", null);

        // Log the method's result, close the database connection, and refresh the cursor
        Log.i(TAG, "Deleted " + Utility.trimUnitsWhenSingular(numRowsDeleted, "rows") + " from the Packs database");
        Utility.toast(getApplicationContext(),
                "Successfully deleted "
                + Utility.trimUnitsWhenSingular(numRowsDeleted, "Packs")
                + " from the database!");
        db.close();
        refreshCursor();
    }

    /**
     * Delete a single Pack from the Packs database.
     * @param rowID
     */
    private void deleteDatabaseItem(long rowID) {
        // Log the method's intent
        Log.i(TAG, "Deleting Pack with row ID " + rowID + " from the Packs database...");

        // Prepare the selection statement
        String selection = PackContract.PackEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(rowID) };

        // Access and delete from the database
        SQLiteDatabase db = packDbHelper.getReadableDatabase();
        int numRowsDeleted = db.delete(PackContract.PackEntry.TABLE_NAME, selection, selectionArgs);

        // Log the method's result
        if (numRowsDeleted == 1) {
            Log.i(TAG, "Deleted Pack with row ID " + rowID + " successfully");
            Utility.toast(getApplicationContext(), "Deleted the Pack successfully!");
        } else {
            Log.i(TAG, "Unable to delete Pack with row ID " + rowID);
            Utility.toast(getApplicationContext(), "Unable to delete the Pack...");
        }

        // Close the database connection and refresh the cursor
        db.close();
        refreshCursor();
    }



    /* Methods that manage the context menu */

    /**
     * Called when a context menu for the View <code>v</code> is about to be shown.
     * This is called every time the context menu is about to be shown
     * (unlike <code>onCreateOptionsMenu()</code>).
     * @param menu     The context menu that is being built
     * @param v        The view for which the context menu is being built
     * @param menuInfo Extra information about the item for which the context menu should be shown.
     *                 This information will vary depending on the class of v.
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        Log.i(TAG, "Trying to create the context menu...");

        if (v.getId() == getListView().getId()){
            // Inflate the context menu
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.context_menu_pack, menu);

            // Set the context menu's title to the Pack's name
            // (Get the clicked row ID and Pack name using the MenuInfo object,
            // the ListView's adapter, and the database cursor)
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            int itemPosition = info.position;
            Adapter adapter = getListAdapter();
            long itemID = adapter.getItemId(itemPosition);
            // long itemID2 = info.id; // This is actually the same, and may be simpler to access

            Cursor myCursor = (Cursor) adapter.getItem(itemPosition);
            myCursor.moveToFirst();
            String myPackName = myCursor.getString(1); // Index 1 is the name
            menu.setHeaderTitle(myPackName);

            Log.i(TAG, "Context menu created for Pack \"" + myPackName + "\", row ID " + itemID);
        }
    }

    /**
     * Called whenever an item in a context menu is selected
     * @param item The menu item that was selected
     * @return Return false to allow normal context menu processing to proceed, true to consume it here
     */
    public boolean onContextItemSelected(MenuItem item) {
        // Get the long-clicked row's row ID using the MenuInfo
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        long myRowID = info.id;

        // Log the clicked row ID and the action being responded to
        Log.i(TAG, "\"" + item.getTitle()
                + "\" action pressed in the context menu for row ID " + myRowID);

        switch (item.getItemId()) {
            /* Delete an item */
            case R.id.context_menu_delete:
                deleteDatabaseItem(myRowID);
                return true;
            /* Edit an item */
            case R.id.context_menu_edit:
                editPack(myRowID);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    /**
     * Open <code>AddPackActivity</code> in order to edit a Pack
     * @param myRowID The row ID of the Pack to edit
     */
    private void editPack(long myRowID) {
        Intent intent = new Intent(this, AddPackActivity.class);
        intent.putExtra(AddPackActivity.ROW_ID_KEY, myRowID);
        startActivity(intent);
    }



    /* Methods that manage the options menu */

    /**
     * Initialize the activity's option menu
     * @param menu The options menu in which to place items
     * @return True if the menu is to be displayed, false otherwise
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present
        getMenuInflater().inflate(R.menu.list_packs, menu);
        return true;
    }

    /**
     * Called whenever an item in the options menu is selected
     * @param item The menu item that was selected
     * @return Return false to allow normal menu processing to proceed, true to consume it here
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        /* Settings */
        if (id == R.id.action_settings) {
            // TODO
            return true;
        }
        /* Delete all */
        else if (id == R.id.action_delete_all_packs) {
            // TODO Display the 'delete all' confirmation screen here?
            deleteAllDatabaseItems();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    /* Other methods */

    /**
     * Show the progress bar animation
     */
    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hide the progress bar animation
     */
    private void hideProgressBar() {
        // Hide the animation and do not allow it to take up any space in the layout
        progressBar.setVisibility(View.GONE);
    }

}
