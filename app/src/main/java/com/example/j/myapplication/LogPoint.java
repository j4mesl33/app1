package com.example.j.myapplication;

import java.util.Date;

/**
 * Class that defines a log point object.
 *
 * A log point object stores something that should be written in the log,
 * such as the creation of a new Item or Pack, or the addition of an Item to a Pack.
 * A collection of log points is a history of actions involving Items or Packs,
 * or other parts of the application.
 * @author James
 */
public class LogPoint {

    /**
     * The date and time when this log point was created
     */
    private Date dateCreated;

    /**
     * The message for this log point
     */
    private String message;

    /**
     * The name of Pack A, if a pack is involved in this log point
     */
    private String namePackA;

    /**
     * The name of Pack B, if a second pack is involved in this log point
     */
    private String namePackB;

    /**
     * The name of Item A, if an item is involved in this log point
     */
    private String nameItemA;

    /**
     * The name of Item B, if a second item is involved in this log point
     */
    private String nameItemB;

    /**
     * Constructs a new log point
     */
    public LogPoint() {
        this("", "", "", "", "");
    }

    /**
     * Constructs a new log point with the given message
     * @param message The message for this log point
     */
    public LogPoint(String message) {
        this(message, "", "", "", "");
    }

    /**
     * Constructs a new log point with the given creation time and message
     * @param creationTime The creation time for this log point, in millis
     * @param message The message for this log point
     */
    public LogPoint(long creationTime, String message) {
        this.dateCreated = new Date(creationTime);
        this.message = message;
        this.namePackA = "";
        this.namePackB = "";
        this.nameItemA = "";
        this.nameItemB = "";
    }

    /**
     * Constructs a new log point involving one item and one pack,
     * with the given message
     * @param message The message for this log point
     * @param namePackA The name of the pack
     * @param nameItemA The name of the item
     */
    public LogPoint(String message, String namePackA, String nameItemA) {
        this(message, namePackA, "", nameItemA, "");
    }

    /**
     * Constructs a new log point involving two items and two packs,
     * with the given message
     * @param message The message for this log point
     * @param namePackA The name of the first pack
     * @param namePackB The name of the second pack
     * @param nameItemA The name of the first item
     * @param nameItemB The name of the second item
     */
    public LogPoint(String message,
                    String namePackA,
                    String namePackB,
                    String nameItemA,
                    String nameItemB) {
        this.message = message;
        this.namePackA = namePackA;
        this.namePackB = namePackB;
        this.nameItemA = nameItemA;
        this.nameItemB = nameItemB;
    }

    /**
     * <b>Note, this String representation is subject to change</b>
     * @return A String representation of a log point
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Time in millis: " + dateCreated.getTime() + "\n");
        sb.append("Message: " + message + "\n");
        return sb.toString();
    }

}
