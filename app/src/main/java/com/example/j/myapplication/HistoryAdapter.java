package com.example.j.myapplication;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.j.myapplication.db.PackContract;

public class HistoryAdapter extends CursorAdapter {

    Context context;
    LayoutInflater inflater;

    /**
     * Recommended constructor
     *
     * @param context The context
     * @param c       The cursor from which to get the data
     * @param flags   Flags used to determine the behavior of the adapter; may
     *                be any combination of {@link #FLAG_AUTO_REQUERY} and
     *                {@link #FLAG_REGISTER_CONTENT_OBSERVER}.
     */
    public HistoryAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Makes a new view to hold the data pointed to by cursor
     *
     * @param context Interface to application's global information
     * @param cursor  The cursor from which to get the data. The cursor is already
     *                moved to the correct position.
     * @param parent  The parent to which the new view is attached to
     * @return The newly created view
     */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(R.layout.custom_row_log, parent, false);
    }

    /**
     * Bind an existing view to the data pointed to by cursor
     *
     * @param view    Existing view, returned earlier by newView
     * @param context Interface to application's global information
     * @param cursor  The cursor from which to get the data
     */
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Get hooks to the TextViews
        TextView logMessage = (TextView) view.findViewById(R.id.custom_row_log_message);
        TextView logCreationTime = (TextView) view.findViewById(R.id.custom_row_log_creation_time);

        // Set the log message
        logMessage.setText(cursor.getString(cursor.getColumnIndexOrThrow(PackContract.PackEntry.COLUMN_NAME_PACK_NAME)));

        // Set the log creation time
        logCreationTime.setText(cursor.getString(cursor.getColumnIndexOrThrow(PackContract.PackEntry.COLUMN_NAME_NOTES)));
    }
}