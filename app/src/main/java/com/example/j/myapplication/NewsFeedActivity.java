package com.example.j.myapplication;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.example.j.myapplication.db.LogContract;
import com.example.j.myapplication.db.LogDbHelper;

/*
  Methods organized: 12/5/14

  + Display the history log items, sorted by creation time (TODO Check this in emulator)
  + Initialization organized with a getUiHooks() method
  + Buttons to launch ListItems and ListPacks, to view all of the stored Items and Packs
  + Options menu: 'Settings'
  + Show a progress bar while loading the cursor data

  TODO
  - onResume, refreshCursor, onCreateContextMenu, onContextItemSelected,
    onListItemClick, hide/delete a log point, jump to the relevant Item or Pack
  - (See {@link ListPacksActivity}

*/

/**

  The main activity that appears when the application is first launched.
  It allows you to navigate to the other activities.

                                            <br><br>
    Constants:                              {@link #TAG}<br>
    Member variables:                       {@link #logDbHelper}<br>
    UI elements:                            {@link #buttonListItems}<br><br>

    Android lifecycle methods:              {@link #onCreate(Bundle)}<br>
    Initialization methods:                 {@link #getUiHooks()}<br>
    Cursor loader methods:                  {@link #onCreateLoader(int, Bundle)}<br>
    Methods that respond to user clicks:    {@link #onClickListItems(View)}<br>
    Methods that manage the options menu:   {@link #onCreateOptionsMenu(Menu)}<br>
    Other methods:                          {@link #showProgressBar()}<br><br>

  @author James L
 */
public class NewsFeedActivity extends ListActivity
        implements LoaderManager.LoaderCallbacks<Cursor> { /** For the database cursor */

    /* Constants */

    /**
     * Android logging tag
     */
    private static final String TAG = "NewsFeedActivity";

    /**
     * The default sort order for the History database query results
     */
    private static final String DEFAULT_SORT_ORDER = LogContract.LogEntry.COLUMN_NAME_CREATION_TIME;

    /**
     * Identifies the loader used to load the History database cursor
     */
    private static final int HISTORY_LOADER = 0;



    /* Member variables */

    /**
     * The database helper for the Log database
     */
    private LogDbHelper logDbHelper;

    /**
     * The view group that stores this activity's views
     */
    private RelativeLayout layout;

    /**
     * The list view that stores the database items
     */
    private ListView listView;

    /**
     * The adapter used to display the list's data
     */
    private CursorAdapter mAdapter;

    /**
     * The progress bar that displays while loading from the database
     */
    private ProgressBar progressBar;



    /* UI elements */

    /**
     * Button that launches ListItemsActivity
     */
    Button buttonListItems;

    /**
     * Button that launches ListPacksActivity
     */
    Button buttonListPacks;



    /* Android lifecycle methods */

    /**
     * Called when the activity is starting
     * @param savedInstanceState If the activity is being re-initialized after previously being shut
     *                           down, then this Bundle contains the data it most recently supplied
     *                           in onSaveInstanceState(Bundle). Otherwise it is null.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_feed);

        // Access the Log database table
        logDbHelper = new LogDbHelper(getApplicationContext());

        // Get UI hooks for the layout, the ListView, and buttons
        getUiHooks();

        // Register the list view for the context menu
        registerForContextMenu(listView);

        // Set the progress bar, already defined in the XML,
        // to display while the list loads
        progressBar = (ProgressBar) findViewById(R.id.progress_history);
        showProgressBar();

        // Set the list view's cursor adapter
        setCursorAdapter();
    }



    /* Initialization methods */

    /**
     * Set references to the UI elements
     */
    private void getUiHooks() {
        // Get UI hooks for the view group layout and for the list view
        layout = (RelativeLayout) findViewById(R.id.layout_news_feed_activity);
        listView = getListView();

        // Get the buttons
        buttonListItems = (Button) findViewById(R.id.button_list_items);
        buttonListPacks = (Button) findViewById(R.id.button_list_packs);
    }

    /**
     * Set the ListView's Cursor adapter
     */
    private void setCursorAdapter() {
        // Set the list view's cursor adapter
        mAdapter = new HistoryAdapter(getApplicationContext(), null, 0);
        setListAdapter(mAdapter);

        // Prepare the loader.
        // Either re-connect with an existing one, or start a new one.
        getLoaderManager().initLoader(HISTORY_LOADER, null, this);
    }



    /* Cursor loader methods */

    /**
     * Instantiate and return a new Loader for the given ID
     * @param loaderID The ID of the loader to be created
     * @param bundle Any arguments supplied by the caller
     * @return A new Loader instance that is ready to start loading
     */
    @Override
    public Loader<Cursor> onCreateLoader(int loaderID, Bundle bundle) {

        return new CursorLoader(this,   // Context
                null,                   // Uri, ok if null
                null,                   // String[] projection
                null,                   // String selection
                null,                   // String[] selectionArgs
                DEFAULT_SORT_ORDER)     // String sortOrder
        {
            @Override
            public Cursor loadInBackground() {
                // Display the progress bar animation by calling
                // the showProgressBar() method from the UI thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showProgressBar();
                    }
                });

                SQLiteDatabase db = logDbHelper.getReadableDatabase();
                return db.query(
                        LogContract.LogEntry.TABLE_NAME,
                        null,                   // Return all columns in the table.
                        null,                   // Return all rows in the table.
                        null,                   // Set to null because 'selection' is also set to null
                        null,                   // Do not group the rows in the result.
                        null,                   // Set to null because 'groupBy' is also set to null.
                        DEFAULT_SORT_ORDER);    // Use the default sort order.
            }
        };

        /**
         * TODO
         * Modify this method to use a switch statement with the loader ID
         * https://developer.android.com/training/load-data-background/setup-loader.html#DefineLaunch
         */
    }

    /**
     * Called when a previously created loader has finished loading
     * @param cursorLoader Class that asynchronously loads the cursor
     * @param cursor       Cursor that provides access to the result set returned by a database query
     */
    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        // Swap the new cursor in
        // (The framework will take care of closing the old cursor once we return)
        mAdapter.swapCursor(cursor);

        // Hide the progress bar animation
        hideProgressBar();
    }

    /**
     * Called when a previously created loader is reset, making the data unavailable
     * @param cursorLoader Class that asynchronously loads the cursor
     */
    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        // This is called when the last Cursor provided to onLoadFinished()
        // above is about to be closed.  We need to make sure we are no
        // longer using it.
        mAdapter.swapCursor(null);
    }



    /* Methods that respond to user clicks */

    /**
     * Responds when the "List Items" button is clicked. Launches <code>ListItemsActivity</code>.
     * @param v The <code>View</code> that was clicked
     */
    public void onClickListItems(View v) {
        Intent intent = new Intent(this, ListItemsActivity.class);
        startActivity(intent);
    }

    /**
     * Responds when the "List Packs" button is clicked. Launches <code>ListPacksActivity</code>.
     * @param v The <code>View</code> that was clicked
     */
    public void onClickListPacks(View v) {
        Intent intent = new Intent(this, ListPacksActivity.class);
        startActivity(intent);
    }



    /* Methods that manage the options menu */

    /**
     * Initialize the activity's option menu
     * @param menu The options menu in which to place items
     * @return True if the menu is to be displayed, false otherwise
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.news_feed, menu);
        return true;
    }

    /**
     * Called whenever an item in the options menu is selected
     * @param item The menu item that was selected
     * @return Return false to allow normal menu processing to proceed, true to consume it here
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    /* Other methods */

    /**
     * Show the progress bar animation
     */
    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hide the progress bar animation
     */
    private void hideProgressBar() {
        // Hide the animation and do not allow it to take up any space in the layout
        progressBar.setVisibility(View.GONE);
    }
}
