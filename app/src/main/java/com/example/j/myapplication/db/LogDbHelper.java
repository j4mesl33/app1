package com.example.j.myapplication.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * The database helper for the Log table
 */
public class LogDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Log.db";

    /**
     * SQL statements that create and delete the Log table
     */
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + LogContract.LogEntry.TABLE_NAME
                    + " (" + LogContract.LogEntry._ID + " INTEGER PRIMARY KEY,"
                    + LogContract.LogEntry.COLUMN_NAME_CREATION_TIME
                    + Db.TEXT_TYPE + Db.COMMA
                    + LogContract.LogEntry.COLUMN_NAME_MESSAGE
                    + Db.TEXT_TYPE + Db.COMMA
                    + LogContract.LogEntry.COLUMN_NAME_PACK_A
                    + Db.TEXT_TYPE + Db.COMMA
                    + LogContract.LogEntry.COLUMN_NAME_PACK_B
                    + Db.TEXT_TYPE + Db.COMMA
                    + LogContract.LogEntry.COLUMN_NAME_ITEM_A
                    + Db.TEXT_TYPE + Db.COMMA
                    + LogContract.LogEntry.COLUMN_NAME_ITEM_B
                    + Db.TEXT_TYPE + " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + LogContract.LogEntry.TABLE_NAME;

    public LogDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p/>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database
     * @param oldVersion The old database version
     * @param newVersion The new database version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
