package com.example.j.myapplication;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.example.j.myapplication.db.BelongsToContract;
import com.example.j.myapplication.db.BelongsToDbHelper;
import com.example.j.myapplication.db.ItemContract;
import com.example.j.myapplication.db.ItemDbHelper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/*
 Everything in this class last accounted for: TODO

 - Create a switch statement to create the requested loader


 */


/**
 * An activity that displays a list of the Items stored in the database.
 * Allows the user to launch <code>AddItemActivity</code> to create and save a new Item,
 * or click a list item to view it in more detail with <code>PackDetailActivity</code>.
 */
public class ListItemsActivity extends ListActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    /**
     * Android logging tag
     */
    private static final String TAG = "ListItemsActivity";

    /**
     * The columns to select from the database table
     * TODO Refactor and rename this PROJECTION_ALL_COLUMNS instead, and likewise for the other activities
     */
    private static final String[] PROJECTION = {
            ItemContract.ItemEntry._ID,
            ItemContract.ItemEntry.COLUMN_NAME_ITEM_NAME,
            ItemContract.ItemEntry.COLUMN_NAME_CATEGORY,
            ItemContract.ItemEntry.COLUMN_NAME_WEIGHT,
            ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_UNITS,
            ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_IN_GRAMS,
            ItemContract.ItemEntry.COLUMN_NAME_QUANTITY,
            ItemContract.ItemEntry.COLUMN_NAME_IS_ESSENTIAL };

    /**
     * The columns to select from the database table: only the ID and the name columns
     */
    private static final String[] PROJECTION_NAME_COLUMN_ONLY = {
            ItemContract.ItemEntry._ID,
            ItemContract.ItemEntry.COLUMN_NAME_ITEM_NAME };
 
    /**
     * The default sort order for the Item database query results
     * TODO Refactor and rename this SORT_BY_NAME or DEFAULT_SORT_BY_NAME
     */
    private static final String DEFAULT_SORT_ORDER = ItemContract.ItemEntry.COLUMN_NAME_ITEM_NAME;

    /**
     * Identifies the loader used to load the Items database cursor
     */
    private static final int ITEMS_LOADER = 0;

    /**
     * The database helper for the Item database
     */
    private ItemDbHelper itemDbHelper;

    /**
     * The database helper for the "belongs-to" database, which identifies Items that belong to Packs
     * TODO Is this a more elegant name to use?
     */
    private BelongsToDbHelper belongsToDbHelper;

    /**
     * The view group that stores this activity's views
     */
    private RelativeLayout layout;

    /**
     * The ListView that stores the database items
     */
    private ListView listView;

    /**
     * The adapter used to display the list's data
     */
    private CursorAdapter mAdapter;

    /**
     * The progress bar that displays while loading from the database
     */
    private ProgressBar progressBar;

   /**
     * The key used to pass the Pack ID of the Pack whose Items should be displayed. This is only
     * used when running in selectable mode. TODO Clarify, and document the two modes more clearly
     */
    public static final String PACK_ID_KEY = "PackId";

    /**
     * The value for the row ID if a value was not received as an Intent extra
     */
    private static final long ROW_ID_NOT_RECEIVED = -1;

    /**
     * The Pack ID of the Pack whose Items should be displayed. This is only used when running in
     * "selectable" mode.
     */
    private long packID;

    /**
     * Whether this activity should run in "selectable" mode, where multiple items may
     * be checked with a check box
     */
    private boolean selectable;

     /**
     * TODO
     */
    private long[] rowIdsCurrentlySelected;

    /**
     * The menu bar
     */
    private Menu menu;



    /* ----------Android activity lifecycle methods---------- */

    /**
     * Called when the activity is starting
     * @param savedInstanceState If the activity is being re-initialized after previously being shut
     *                           down, then this Bundle contains the data it most recently supplied
     *                           in onSaveInstanceState(Bundle). Otherwise it is null.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_items);

        // Access the two databases, for the Items table, and for the "belongs-to" table
        itemDbHelper = new ItemDbHelper(getApplicationContext());
        belongsToDbHelper = new BelongsToDbHelper(getApplicationContext());

        // Get UI hooks for the layout and for the list view
        layout = (RelativeLayout) findViewById(R.id.list_of_items);
        listView = getListView();

        // Register the list view for the context menu
        registerForContextMenu(listView);

        // Set the progress bar, already defined in the XML,
        // to display while the list loads
        // TODO Get the UI hooks and find the progress bar in a separate class that initializes these things?
        progressBar = (ProgressBar) findViewById(R.id.progress_items);
        showProgressBar();

        // Register the list view for the context menu
        mAdapter = new ItemAdapter(getApplicationContext(), null, 0);
        setListAdapter(mAdapter);

        // Prepare the loader.
        // Either re-connect with an existing one, or start a new one.
        getLoaderManager().initLoader(ITEMS_LOADER, null, this);

        // Customize the activity differently for normal mode and for selectable mode
        checkSelectableMode();

        Log.i(TAG, "Activity created");
    }

    /**
     * Called when the activity is being resumed
     */
    @Override
    protected void onResume() {
        // This lifecycle method is called:
        // - when this activity is first created,
        // - when this activity returns to the foreground (partially obscured), and
        // - when this activity is opened again after having been not-visible (fully obscured)

        // Need to make sure that refreshing the cursor in this method is not redundant,
        // i.e. when first started, onCreate() will read from the database, and this method will
        // then refresh a just-loaded cursor. Use logging to figure out exactly what calls are made,
        // and when.

        // Could pass an activity result back to this activity after creating a new Pack,
        // so that this activity knows to refresh.
        // (With this approach, the cursor would not be refreshed if the user started to create a
        // new Pack but then cancelled. However, in the future, when server functionality is added,
        // and when multiple users may be making updates, it might be nice to simply always refresh
        // on resume because another user may have added an item in the time that the first user
        // spent starting to create an item and then cancelling.)
        super.onResume();

        refreshCursor();
    }










    /* ----------TODO Methods that I'm currently working on---------- */

    /**
     * Checks the Intent extras for a Pack ID passed by the <code>PACK_ID_KEY</code>
     * If a Pack ID was not passed, customize the activity for the normal mode.
     * If a Pack ID was passed, customize the activity for selectable mode.
     */
    private void checkSelectableMode() {
        Intent intent = getIntent();

        selectable = intent.hasExtra(PACK_ID_KEY);
        if (!selectable) { /* Normal mode */

            // TODO
            
        } else { /* Selectable mode, where the user may tick checkboxes next to each Item */

            // Get the Pack ID
            packID = intent.getLongExtra(PACK_ID_KEY, ROW_ID_NOT_RECEIVED);

            // Hide the Button button_add_item
            Button addItemButton = (Button) findViewById(R.id.button_add_item);
            addItemButton.setVisibility(View.GONE);
            
            // Turn on the selectable mode
            listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
 
            // Turn off the "delete all rows" menu option
            MenuItem item = (MenuItem) findViewById(R.id.action_delete_all_rows);
            item.setEnabled(false);

            // Put a "Done" button, to press after checking and un-checking some items, and a "Cancel" button
            // Put a "Done" and "Cancel" button in the XML that is normally hidden
            showDoneCancelButtons();

            // Tick the items that were previously checked in this activity
            rowIdsCurrentlySelected = getSelectedItemIds(packID, getApplicationContext());
            tickCheckedItems(rowIdsCurrentlySelected);

            /*
             Steps:

             - Read the Pack ID from the DB
             - Get a list of all the Items that belong in this Pack, from the "belongs to" database (ITEMS-IN-PACK)
             - Display a list of all of the Items (ALL-ITEMS)
             - Tick the checkboxes next to each of ITEMS-IN-PACK

             To update the DB:
             
               There are 2 different lists:
               - PREVIOUSLY-TICKED
               - CURRENTLY-TICKED

               "Items to add" = CURRENTLY-TICKED - PREVIOUSLY-TICKED
               "Items to delete" = PREVIOUSLY-TICKED - CURRENTLY-TICKED


             Pick a single word and stick with it: "ticked", "checked", "selected"

             */

        }
    }

    /**
     * Set the visibility of the 'Done' and 'Cancel' buttons to visible and clickable
     */
    private void showDoneCancelButtons() {
        MenuItem doneAction = menu.findItem(R.id.action_done_selecting);
        MenuItem cancelAction = menu.findItem(R.id.action_cancel_selecting);
        doneAction.setVisible(true);
        cancelAction.setVisible(true);
    }

    /**
     * Respond to the button press on the 'Done' button when in selectable mode
     * @param v
     * TODO This needs a better name
     */
    public void getItemsChecked(View v) {
        // Get the IDs of the Items that were previously checked, and make sure to tick their checkboxes
        long[] checkedItemIDs = getSelectedItemIds(packID, getApplicationContext());
        tickCheckedItems(checkedItemIDs);

        // Do the actual database updates
        updateBelongsToDatabase(checkedItemIDs);
    }

    /**
     * Helper method: Given a <code>long[]</code> of Item IDs, tick each of those Items in the ListView checkbox display
     * @param itemIDs The IDs of the Items to tick in the checkbox display
     */
    private void tickCheckedItems(long[] itemIDs) {
        // For each itemID, get its position, and set that position to 'checked'
        for (long itemID : itemIDs) {
            int position = getAdapterItemPosition(itemID);
            listView.setItemChecked(position, true);
        }
    }

    /**
     * Static utility method: Given a Pack ID, return a <code>long[]</code> that stores the Item IDs of the Items that
     * already belong in this Pack. These are the Items that were previously ticked by the user in
     * this activity, and their checkboxes should already be ticked when the user returns to this
     * activity.
     * @param packID The Pack ID whose Items to identify
     * @return A <code>long[]</code> of the Item IDs that belong to the Pack
     */
    private static long[] getSelectedItemIds(long packID, Context context) {
        // Design decision: Make this a static method instead of an instance method.
        // Also consider whether this should be placed in a different class.

        // Create a database helper and access the database in readable mode
        BelongsToDbHelper belongsToDbHelper = new BelongsToDbHelper(context);
        SQLiteDatabase db = belongsToDbHelper.getReadableDatabase();

        // Create the arrays that define the projection and selection statements
        String[] projection = { BelongsToContract.BelongsToEntry.COLUMN_NAME_ITEM_ID };
        String selection = "WHERE " + BelongsToContract.BelongsToEntry.COLUMN_NAME_PACK_ID + " = ?";
        String[] selectionArg = { String.valueOf(packID) };

        // Query the database
        Cursor c = db.query(
                BelongsToContract.BelongsToEntry.TABLE_NAME,
                projection,
                selection,
                selectionArg,
                null,
                null,
                null);

        int rowCount = c.getCount();
        long[] toReturn = new long[rowCount];
        Log.i(TAG, "Pack ID " + packID + " has " + rowCount + " Items");

        c.moveToFirst();

        if (rowCount == 0) {
            Utility.toast(context, "No items in this pack!");
        } else {
            int i = 0;
            while (!c.isAfterLast()) {
                toReturn[i++] = c.getLong(c.getColumnIndexOrThrow(BelongsToContract.BelongsToEntry.COLUMN_NAME_ITEM_ID));
                c.moveToNext();
            }
        }
        return toReturn;
    }

    /**
     * Helper method: Given the ID of an adapter item, return its position in the adapter
     * @param id The ID of an adapter item
     * @return The item's position
     */
    private int getAdapterItemPosition(long id) {
        for (int position = 0; position < mAdapter.getCount(); position++) {
            if (mAdapter.getItemId(position) == id) {
                return position;
            }
        }
        return 0;
    }


    /* ----------Methods that read from and write to the 'belongs to' database---------- */

    /**
     * Update the database that stores how Items "belong to" Packs
     * TODO This item is named "update", but it doesn't actually use the db.update() method,
     *      it uses db.insert() and db.delete(). Rename to avoid confusion.
     * @param
     */
    private void updateBelongsToDatabase(long[] newRowIds) {
        SQLiteDatabase db = belongsToDbHelper.getWritableDatabase();

        // The old row IDs
        Set hashSetOldRowIds = new HashSet(Arrays.asList(rowIdsCurrentlySelected));
        // Check that the long[] is automatically boxed into a List<Long> TODO

        // The new row IDs
        Set hashSetNewRowIds = new HashSet(Arrays.asList(newRowIds));
        // TODO Here too


        // For each old row ID, if it is not also found in 'hashSetNew', remove it.
        for (long itemRowID : rowIdsCurrentlySelected) {
            String selection = BelongsToContract.BelongsToEntry.COLUMN_NAME_ITEM_ID + " LIKE ?";
            String[] selectionArg = { String.valueOf(itemRowID) };
            db.delete(BelongsToContract.BelongsToEntry.TABLE_NAME, selection, selectionArg);
            Log.i(TAG, "'Item belongs-to Pack' relationship removed, item row ID: " + itemRowID);
        }

        // For each new row ID, if it is not also found in 'hashSetOld', add it.
        for (long itemRowID : newRowIds){
            ContentValues values = new ContentValues();
            values.put(BelongsToContract.BelongsToEntry.COLUMN_NAME_PACK_ID, packID);
            values.put(BelongsToContract.BelongsToEntry.COLUMN_NAME_ITEM_ID, itemRowID);
            long newItemRowID = db.insert(
                    BelongsToContract.BelongsToEntry.TABLE_NAME,
                    null, // TODO
                    values);
            Log.i(TAG, "New 'Item belongs-to Pack' relationship added, row ID: " + newItemRowID);
        }

    }










    /* ----------Methods that initialize and respond to options menu items---------- */

    /**
     * Initialize the activity's option menu
     * @param menu The options menu in which to place items
     * @return True if the menu is to be displayed, false otherwise
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present
        getMenuInflater().inflate(R.menu.list_items, menu);
        this.menu = menu;
        return true;
    }

    /**
     * Called whenever an item in the options menu is selected
     * @param item The menu item that was selected
     * @return Return false to allow normal menu processing to proceed, true to consume it here
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            // Open the settings menu here
            // openSettings(), or something like that
            return true;
        } else if (id == R.id.action_delete_all_rows) {
            // Display a confirmation window that asks, "Yes, delete all rows", or "Cancel"
            optionDeleteAllRows();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Respond to the options menu action "Delete all rows"
     */
    private void optionDeleteAllRows() {
        new AlertDialog.Builder(this)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle("Delete all?") // TODO Factor out strings to resource file
            .setMessage("Are you sure you want to delete all items?")
            .setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    deleteAllDatabaseItems();
            }

        })
        .setNegativeButton("No", null)
        .show();
    }



    /* ----------Methods that manage the Loader---------- */

    /**
     * Instantiate and return a new Loader for the given ID
     * @param loaderID The ID of the loader to be created
     * @param bundle Any arguments supplied by the caller
     * @return A new Loader instance that is ready to start loading
     */
    @Override
    public Loader<Cursor> onCreateLoader(int loaderID, Bundle bundle) {

        return new CursorLoader(this,               // Context
                null,                               // Uri, ok if null
                PROJECTION,                         // String[] projection
                null,                               // String selection
                null,                               // String[] selectionArgs
                DEFAULT_SORT_ORDER)                 // String sortOrder
        {
            @Override
            public Cursor loadInBackground() {
                // Display the progress bar animation
                // by calling the showProgressBar method from the UI thread
                runOnUiThread(new Runnable() {
                      @Override
                      public void run() {
                          showProgressBar();
                      }
                });

                SQLiteDatabase db = itemDbHelper.getReadableDatabase();
                return db.query(
                        ItemContract.ItemEntry.TABLE_NAME,
                        PROJECTION,
                        null,       // Return all rows in the table.
                        null,       // Set to null because 'selection' is also set to null
                        null,       // Do not group the rows in the result.
                        null,       // Set to null because 'groupBy' is also set to null.
                        DEFAULT_SORT_ORDER);      // Use the default sort order. TODO This was changed from null
            }
        };

        /*
        TODO
        In the future, create a ContentProvider to share data with other
        applications, and to use in this method.
        https://stackoverflow.com/questions/18326954/how-to-read-an-sqlite-db-in-android-with-a-cursorloader
         */

        /**
         * TODO
         * Modify this method to use a switch statement with the loader ID
         * https://developer.android.com/training/load-data-background/setup-loader.html#DefineLaunch
         */
    }

    /**
     * Called when a previously created loader has finished loading
     * @param cursorLoader Class that asynchronously loads the cursor
     * @param cursor       Cursor that provides access to the result set returned by a database query
     */
    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        // Swap the new cursor in.
        // (The framework will take care of closing the old cursor once we return)
        mAdapter.swapCursor(cursor);

        // Hide the progress bar animation
        hideProgressBar();
    }

    /**
     * Called when a previously created loader is reset, making the data unavailable
     * @param cursorLoader Class that asynchronously loads the cursor
     */
    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        // This is called when the last Cursor provided to onLoadFinished()
        // above is about to be closed.  We need to make sure we are no
        // longer using it.
        mAdapter.swapCursor(null);
    }

    /**
     * Refresh the database cursor
     */
    private void refreshCursor() {
   	    getLoaderManager().restartLoader(ITEMS_LOADER, null, this);
    }



    /* ----------Methods that respond to clicks---------- */

    /**
     * Respond to the "Add item" button press and start AddItemActivity
     * @param view
     */
    public void addItem(View view) {
        // Start the AddItemActivity and make a request to receive the new Item
        Intent intent = new Intent(this, AddItemActivity.class);
        startActivity(intent);
        // startActivityForResult(intent, NEW_ITEM_REQUEST); TODO Delete me after refactoring too
    }

    /**
     * Respond when a list item is clicked
     * @param l        The ListView
     * @param v
     * @param position The position of the item that was clicked
     * @param id
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        long rowID = l.getAdapter().getItemId(position);
        Log.i(TAG, "Position " + position + ", row ID " + rowID
                + " in the ListView was clicked");

        Log.i(TAG, "Attempting to retrieve the Item from the ListView's adapter...");

        // Access the data associated with the selected item
        Cursor cursor = (Cursor) (l.getAdapter().getItem(position));

        Utility.toast(getApplicationContext(), "Cursor: " + cursor.getString(1)); // TODO Column number
        Log.i(TAG, "Cursor: " + cursor.getString(1));

        goToListItemDetailPage(id);
    }

    /**
     * TODO
     */
    private void goToListItemDetailPage(long rowID) {
        // Take the row ID of the clicked Item and open a detail view page for that Item
        Intent intent = new Intent(this, ItemDetailActivity.class);
        intent.putExtra(ItemDetailActivity.ROW_ID_KEY, rowID);
        startActivity(intent);
    }

    /**
     * TODO Remove this method, and use Activity.registerForContextMenu(listView) instead
     */
    @Deprecated
    private void setLongClickListener() {
        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> av, View v, int position, long id) {
                // Get your item here with the position
                Log.i(TAG, "Position " + position
                        + ", row ID in the ListView was long-clicked");

                // Create context menu with options for row ID 'id'
                // id;

                // Use the long id to delete the selected item from the database
                deleteDatabaseItem(id);
            return true;
            }
        });
    }



    /* ----------Methods that write changes to the Items database---------- */

    /**
     * Delete all Items from the Items database
     */
    private void deleteAllDatabaseItems() {
        Log.i(TAG, "Deleting all Items from the Items database...");
        SQLiteDatabase db = itemDbHelper.getReadableDatabase();
        int numRowsDeleted = db.delete(ItemContract.ItemEntry.TABLE_NAME, "1", null);
        Log.i(TAG, "Deleted " + numRowsDeleted + " rows from the Items database");
        db.close();
        refreshCursor();
    }

    /**
     * Delete a single Item from the Items database
     * @param rowID
     */
    private void deleteDatabaseItem(long rowID) {
        Log.i(TAG, "Deleting Item with row ID " + rowID + " from the Items database...");
        String selection = ItemContract.ItemEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(rowID) };
        SQLiteDatabase db = itemDbHelper.getReadableDatabase();
        int numRowsDeleted = db.delete(ItemContract.ItemEntry.TABLE_NAME, selection, selectionArgs);
        // TODO Check the different return values better, and document the different results

        if (numRowsDeleted == 1) {
            Log.i(TAG, "Deleted Item with row ID " + rowID + " successfully");
        } else {
            Log.i(TAG, "Unable to delete Item with row ID " + rowID);
        }

        db.close();
        refreshCursor();
    }



    /* ----------Helper methods---------- */

    /**
     * Show the progress bar animation
     */
    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hide the progress bar animation
     */
    private void hideProgressBar() {
        // Hide the animation and do not allow it to take up any space in the layout
        progressBar.setVisibility(View.GONE);
    }

}
