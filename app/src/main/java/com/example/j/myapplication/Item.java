package com.example.j.myapplication;

import java.io.Serializable;

/*
 TODO
 - Add 'is consumable' field
 - Could automatically decrement the quantity of consumable items
   - An 'after the trip' button to keep track of food, toiletries, MREs remaining
 */

/**
 * Represents a single packable item, such as a tent, a rain jacket,
 * a meal item, or a sleeping bag. Implements the <code>Serializable</code>
 * interface so that it can be passed as an Intent extra.
 * @author James
 */
public class Item implements Serializable {

    public static final String GRAMS = "Grams";
    public static final String KILOGRAMS = "Kilograms";
    public static final String OUNCES = "Ounces";
    public static final String POUNDS = "Pounds";

    public static final int GRAMS_IN_ONE_KILOGRAM = 1000;
    public static final int GRAMS_IN_ONE_OUNCE = 28; // Actually 28.3495 TODO Multiplication with doubles?
    public static final int GRAMS_IN_ONE_POUND = 454; // Actually 453.592

    /**
     * The name of the item
     */
    private String itemName;

    /**
     * The item's category
     */
    private String itemCategory;

    /**
     * The item's weight
     */
    private int itemWeight;

    /**
     * The units used for the item's weight
     */
    private String itemWeightUnits;

    /**
     * The item's weight in grams
     */
    private int itemWeightInGrams;

    /**
     * The quantity of this item
     */
    private int itemQuantity;

    /**
     * Whether this item is considered an essential
     */
    private boolean itemIsEssential;

    /**
     * Item notes
     */
    private String itemNotes;

    /**
     * Item photo file path
     */
    private String itemPhotoFilePath;

    /**
     * Default constructor for an Item
     */
    public Item() {
        itemName = "";
        itemCategory = "";
        itemWeight = 1;
        itemWeightUnits = "Grams";
        itemQuantity = 1;
        itemIsEssential = false;
        itemWeightInGrams = convertWeightToGrams(itemWeight, itemWeightUnits);
        itemNotes = "";
        itemPhotoFilePath = "";
    }

    /**
     * Constructs a new Item with the given properties
     * @param itemName The name of the item
     * @param itemCategory The item's category
     * @param itemWeight The item's weight
     * @param itemWeightUnits The units used for the item's weight
     * @param itemQuantity The quantity of the item
     * @param itemIsEssential Whether the item is considered an essential
     * @param itemNotes Item notes
     */
    public Item(String itemName,
                String itemCategory,
                int itemWeight,
                String itemWeightUnits,
                int itemQuantity,
                boolean itemIsEssential,
                String itemNotes) {
        this.itemName = itemName;
        this.itemCategory = itemCategory;
        this.itemWeight = itemWeight;
        this.itemWeightUnits = itemWeightUnits;
        this.itemWeightInGrams = convertWeightToGrams(itemWeight, itemWeightUnits);
        this.itemQuantity = itemQuantity;
        this.itemIsEssential = itemIsEssential;
        this.itemNotes = itemNotes;
        this.itemPhotoFilePath = "";
    }


    /**
     * Constructs a new Item with the given properties
     * @param itemName The name of the item
     * @param itemCategory The item's category
     * @param itemWeight The item's weight
     * @param itemWeightUnits The units used for the item's weight
     * @param itemQuantity The quantity of the item
     * @param itemIsEssential Whether the item is considered an essential
     * @param itemNotes Item notes
     * @param itemPhotoFilePath Item photo file path
     */
    public Item(String itemName,
                String itemCategory,
                int itemWeight,
                String itemWeightUnits,
                int itemQuantity,
                boolean itemIsEssential,
                String itemNotes,
                String itemPhotoFilePath) {
        this.itemName = itemName;
        this.itemCategory = itemCategory;
        this.itemWeight = itemWeight;
        this.itemWeightUnits = itemWeightUnits;
        this.itemWeightInGrams = convertWeightToGrams(itemWeight, itemWeightUnits);
        this.itemQuantity = itemQuantity;
        this.itemIsEssential = itemIsEssential;
        this.itemNotes = itemNotes;
        this.itemPhotoFilePath = itemPhotoFilePath;
    }

    /**
     * @return The name of the item
     */
    public String getName() {
        return itemName;
    }

    /**
     * @return The item's category
     */
    public String getCategory() { return itemCategory; }

    /**
     * @return The item's weight
     */
    public int getWeight() { return itemWeight; }

    /**
     * @return The units used for the item's weight
     */
    public String getWeightUnits() { return itemWeightUnits; }

    /**
     * @return The item's weight in grams
     */
    public int getWeightInGrams() {
        return itemWeightInGrams;
    }

    /**
     * Takes the item's weight and converts it to grams
     * @param itemWeight The item's weight
     * @param itemWeightUnits The units used for the item's weight
     * @return The item's weight in grams
     */
    private int convertWeightToGrams(int itemWeight, String itemWeightUnits) {
        if (itemWeightUnits.equals(GRAMS)) {
            return itemWeight;
        } else if (itemWeightUnits.equalsIgnoreCase(KILOGRAMS)) {
            return itemWeight * GRAMS_IN_ONE_KILOGRAM;
        } else if (itemWeightUnits.equalsIgnoreCase(OUNCES) || itemWeightUnits.equals("Oz")) {
            return itemWeight * GRAMS_IN_ONE_OUNCE;
        } else if (itemWeightUnits.equalsIgnoreCase(POUNDS)) {
            return itemWeight * GRAMS_IN_ONE_POUND;
        } else {
            throw new IllegalArgumentException("Unknown weight units: \"" + itemWeightUnits + "\"");
        }
    }

    /**
     * @return The quantity of the item
     */
    public int getQuantity() { return itemQuantity; }

    /**
     * @return Whether the item is considered an essential
     */
    public boolean getIsEssential() { return itemIsEssential; }

    /**
     * @return Item notes
     */
    public String getNotes() { return itemNotes; }

    /**
     * @return Item photo file path
     */
    public String getPhotoFilePath() { return itemPhotoFilePath; }

    /**
     * Set the Item's photo file path
     */
    public void setItemPhotoFilePath(String itemPhotoFilePath) { this.itemPhotoFilePath = itemPhotoFilePath; }

    /**
     * @return True if the Item has a photo stored in the photo directory, false if it does not
     */
    public boolean hasPhoto() { return itemPhotoFilePath.equals(""); }

    /**
     * @return A String representation of the Item
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Item: " + itemName + "\n");
        sb.append("Quantity: " + itemQuantity + "\n");
        sb.append("Category: " + itemCategory + "\n");

        sb.append("Weight: " + itemWeight + " " + itemWeightUnits);
        if (!itemWeightUnits.equals("grams")) {
            sb.append(" (" + itemWeightInGrams + " grams)");
        }
        sb.append("\n");

        if (itemIsEssential) {
            sb.append("An essential item\n");
        } else {
            sb.append("A non-essential item\n");
        }

        sb.append("Notes: \"" + itemNotes + "\"\n");

        if (!hasPhoto()) {
            sb.append("No photo\n");
        } else {
            sb.append("Photo: " + itemPhotoFilePath + "\n");
        }

        return sb.toString();
    }
}
