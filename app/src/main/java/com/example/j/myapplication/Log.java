package com.example.j.myapplication;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.j.myapplication.db.LogContract;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 */
public class Log {

    private static final String DEFAULT_SORT_ORDER = "";

    /**
     * Get the whole log (the entire history, all of the log points) as a List
     * @return List of LogPoints, sorted by creation time, descending (from the current time, backwards)
     */
    public static List<LogPoint> getLog(SQLiteDatabase db) {
        // Query the database and get a cursor
        Cursor c = db.query(
                LogContract.LogEntry.TABLE_NAME,
                null,                   // Select all columns.
                null,                   // Select all rows.
                null,                   // Null also.
                null,                   // Do not group the rows in the result.
                null,                   // Null also.
                DEFAULT_SORT_ORDER);    // Sort by the creation time, descending

        // Create a new list to be returned
        List<LogPoint> history = new ArrayList<LogPoint>();

        // Figure out how many rows were returned
        int numRows = c.getCount();
        if (numRows == 0) {
            // Return the empty list
            return history;
        }

        // Add a LogPoint to the List for each row read from the cursor
        c.moveToFirst();
        while (!c.isAfterLast()) {
            // Read the creation time and the message
            long creationTimeMillis = c.getLong(c.getColumnIndexOrThrow(LogContract.LogEntry.COLUMN_NAME_CREATION_TIME));
            String message = c.getString(c.getColumnIndexOrThrow(LogContract.LogEntry.COLUMN_NAME_MESSAGE));

            // Create a log point object using those read values
            LogPoint point = new LogPoint(creationTimeMillis, message);

            // Add the log point to the list
            history.add(point);

            // Advance the cursor
            c.moveToNext();
        }

        // Return the list of log points
        return history;
    }

    /**
     * Write a message to the log (a single log point)
     * @param db A writable SQLiteDatabase object
     * @param message The message to write
     * @return The row ID of the newly inserted row, or -1 if an error occurred
     */
    public static long log(SQLiteDatabase db, String message) {
        // Create a map of values, with column names as the keys and data attributes as the values
        ContentValues values = new ContentValues();

        long currentTimeMillis = new Date().getTime();
        values.put(LogContract.LogEntry.COLUMN_NAME_CREATION_TIME, currentTimeMillis);
        values.put(LogContract.LogEntry.COLUMN_NAME_MESSAGE, message);

        long newRowId = db.insert(LogContract.LogEntry.TABLE_NAME, null, values);
        return newRowId;
    }

    /*

        // Get the database in write mode
        SQLiteDatabase db = itemDbHelper.getWritableDatabase();

        // Create a map of values, with column names as the keys and data attributes as the values
        ContentValues values = new ContentValues();
        values.put(ItemContract.ItemEntry.COLUMN_NAME_ITEM_NAME, item.getName());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_CATEGORY, item.getCategory());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT, item.getWeight());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_UNITS, item.getWeightUnits());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_IN_GRAMS, item.getWeightInGrams());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_QUANTITY, item.getQuantity());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_IS_ESSENTIAL, item.getIsEssential());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_NOTES, item.getNotes());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_PHOTO_FILE_PATH, item.getPhotoFilePath());

        // Insert the new row
        long newRowId = db.insert(ItemContract.ItemEntry.TABLE_NAME, null, values);

        if (newRowId == -1) {
            android.util.Log.e(TAG, "New Item database insert was unsuccessful!");
        } else {
            Log.i(TAG, "New Item row ID, " + newRowId);
        }
        return newRowId;

     */

    public static boolean logPack(SQLiteDatabase db, String message, String namePack) {
        return false;
    }

    public static boolean logItem(SQLiteDatabase db, String message, String nameItem) {
        return false;
    }

    public static boolean logPackAndItem(SQLiteDatabase db, String message, String namePackA, String nameItemA) {
        return false;
    }

}
