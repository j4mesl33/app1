package com.example.j.myapplication;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;

import com.example.j.myapplication.db.ItemContract;
import com.example.j.myapplication.db.ItemDbHelper;
import com.example.j.myapplication.db.LogDbHelper;

/*
  All methods completely organized:                12/08/14
  Camera functionality last updated:               11/27/14
  Main functionality in this class last updated:   11/20/14

  TODO
  Currently: move the 'Add photo' button to ItemDetail, and do the DB saving there.
  Also, complete updateItemInDatabase()

  TODO
  - updateItemInDatabase()

  - Photo file name numbering
    - This activity uses the row ID to name the image file, which,
      unless using Edit mode, will always be -1, hence Image-1.jpg (negative 1, not dash 1)

  - Allow user to fill out Item information and Submit
  - The next screen allows the user to take a photo
  - The user can always add a photo from the ItemDetailActivity
  - Allow adding a photo in AddItem at all??

  - Doesn't make sense for the default quantity to be 0

 */


/**
  The activity that allows a user to create a new Item. An Item represents an instance of a single
  packable item that can be stored in a backpack. An Item has a weight and a quantity, as well as
  other fields.

                                                        <br><br>
    Constants                                           {@link #TAG}<br>
    Member variables                                    {@link #editMode}<br>
    UI elements                                         {@link #editItemName}<br><br>

    Android activity lifecycle methods                  {@link #onCreate(Bundle)}<br>
    Initialization methods                              {@link #getUiHooks()}<br>
    Methods that respond to user clicks                 {@link #submit(View)}<br>
    Methods that read from the database                 {@link #getExistingItem(long)}<br>
    Methods that write to the database                  {@link #addItemToDatabase(Item)}<br>
    Methods that manage the options menu                {@link #onCreateOptionsMenu(Menu)}<br><br>

  @author James
 */
public class AddItemActivity extends Activity {

    /* Constants */

    /**
     * Android logging tag
     */
    private static final String TAG = "AddItemActivity";

    /**
     * Intent extra key. If this key is received, this activity will edit an existing Item instead
     * of creating a new one from initially blank fields. Used to pass the row ID of the Item that
     * should be displayed.
     */
    public static final String ROW_ID_KEY = "RowID";

    /**
     * The value for the row ID if a value was not received as an Intent extra
     */
    private static final long ROW_ID_NOT_RECEIVED = -1;

    /**
     * Intent extra key. If this key is received, this activity will set the focus to the View that
     * has the <code>int</code> ID given.
     */
    public static final String VIEW_FOCUS_KEY = "ViewFocus";

    /**
     * The maximum value allowable in the quantity number picker
     */
    public static final int NUMBER_PICKER_MAX_VALUE = 99;



    /* Member variables */

    /**
     * Is set to true if the activity should edit an existing Item instead of creating a new Item;
     * false if it should create a new Item, as is normal
     */
    private boolean editMode;

    /**
     * The row ID of the Item in the database to display
     */
    private long rowID;

    /**
     * The database helper for the Item table
     */
    private ItemDbHelper itemDbHelper;



    /* UI elements */

    /**
     * Name
     */
    private EditText editItemName;

    /**
     * Category
     */
    private Spinner editItemCategory;

    /**
     * Weight
     */
    private NumberPicker editItemWeight;

    /**
     * Weight units
     */
    private Spinner editItemWeightUnits;

    /**
     * Quantity
     */
    private NumberPicker editItemQuantity;

    /**
     * "Is essential"
     */
    private CheckBox editItemEssential;

    /**
     * Notes
     */
    private EditText editItemNotes;



    /* Android activity lifecycle methods */

    /**
     * Called when the activity is starting
     * @param savedInstanceState If the activity is being re-initialized after previously being shut
     *                           down, then this Bundle contains the data it most recently supplied
     *                           in onSaveInstanceState(Bundle). Otherwise it is null.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        // Find the input fields, and log error messages if any necessary Views are not found
        getUiHooks();

        // Prepare the database helper
        itemDbHelper = new ItemDbHelper(getApplicationContext());

        // Check the Intent extras to see if the activity should edit an existing Item
        // instead of creating a new Item
        checkEditMode();

        Log.i(TAG, "Activity created");

        // XXX
        /*
        Show the entire history in a toast, for testing purposes

        StringBuilder sb = new StringBuilder();
        List<LogPoint> history = History.getLog(new LogDbHelper(getApplicationContext()).getReadableDatabase());
        for (LogPoint point : history) {
            sb.append(point.toString() + "\n\n");
        }
        Utility.toast(getApplicationContext(), sb.toString());
        */

    }



    /* Initialization methods */

    /**
     * Set references to the UI elements, and log error messages if any necessary Views
     * are not found
     */
    private void getUiHooks() {
        // Get the UI hooks
        editItemName = (EditText) findViewById(R.id.edit_item_name);
        editItemCategory = (Spinner) findViewById(R.id.edit_item_category);
        editItemWeight = (NumberPicker) findViewById(R.id.edit_item_weight);
        editItemWeightUnits = (Spinner) findViewById(R.id.edit_item_weight_units);
        editItemQuantity = (NumberPicker) findViewById(R.id.edit_item_quantity);
        editItemEssential = (CheckBox) findViewById(R.id.edit_item_essential);
        editItemNotes = (EditText) findViewById(R.id.edit_item_notes);

        // Set category spinner
        ArrayAdapter<CharSequence> categoryAdapter = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.item_category_array, android.R.layout.simple_spinner_item);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        editItemCategory.setAdapter(categoryAdapter);

        // Set number picker params
        editItemWeight.setMinValue(0);
        editItemWeight.setMaxValue(NUMBER_PICKER_MAX_VALUE);
        editItemWeight.setWrapSelectorWheel(false);

        // Set weight units spinner
        ArrayAdapter<CharSequence> unitsAdapter = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.weight_units_array, android.R.layout.simple_spinner_item);
        unitsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        editItemWeightUnits.setAdapter(unitsAdapter);

        // Set number picker params
        editItemQuantity.setMinValue(0);
        editItemQuantity.setMaxValue(NUMBER_PICKER_MAX_VALUE);
        editItemQuantity.setWrapSelectorWheel(true);

        // Log error messages if any necessary Views are not found
        if (editItemName == null) {
            Log.e(TAG, "EditText 'edit_item_name' not found!");
        } if (editItemCategory == null) {
            Log.e(TAG, "EditText 'edit_item_category' not found!");
        } if (editItemWeight == null) {
            Log.e(TAG, "EditText 'edit_item_weight' not found!");
        } if (editItemWeightUnits == null) {
            Log.e(TAG, "EditText 'edit_item_weight_units' not found!");
        } if (editItemQuantity == null) {
            Log.e(TAG, "EditText 'edit_item_quantity' not found!");
        } if (editItemEssential == null) {
            Log.e(TAG, "EditText 'edit_item_essential' not found!");
        } if (editItemNotes == null) {
            Log.e(TAG, "EditText 'edit_item_notes' not found!");
        }
    }

    /**
     * Check the Intent extras to see if this activity should edit an existing Item
     * or create a new Item
     */
    private void checkEditMode() {
        // Set edit mode to false by default, and the row ID to -1,
        // to indicate that a row ID was not received
        editMode = false;
        rowID = ROW_ID_NOT_RECEIVED;

        // Read the Item ID from the Intent extra if one was received
        Intent intent = getIntent();
        if (intent.hasExtra(ROW_ID_KEY)) {
            editMode = true;
            rowID = intent.getLongExtra(ROW_ID_KEY, ROW_ID_NOT_RECEIVED);
        }

        // Log the activity's mode and set the appropriate activity title
        if (!editMode) {
            Log.i(TAG, "AddItem will run in 'normal' mode and create a new Item");
        } else {
            Log.i(TAG, "AddItem will run in 'edit' mode and edit an existing Item, row ID " + rowID);
            String myItemName = getExistingItemName(rowID, itemDbHelper.getReadableDatabase());
            getActionBar().setTitle("Edit " + myItemName);
        }

        // If requested, set the focus on the specified focusable View
        boolean shouldFocus = intent.hasExtra(VIEW_FOCUS_KEY);
        if (shouldFocus) {
            int viewID = intent.getIntExtra(VIEW_FOCUS_KEY, 0);
            setFocus(viewID);
        } else if (editMode) {
            // If the activity should edit an existing item,
            // but focus was not requested on a specific view
            Log.i(TAG, "Focus was not requested on a specific view");
        }

        // Finally, when running in edit mode, fill in the input fields with the existing
        // values, so that they can be edited
        if (editMode) {
            fillInputFields();
        }
    }

    /**
     * Set focus on the specified View
     * @param viewID The ID of the View on which to focus
     */
    private void setFocus(int viewID) {
        Log.i(TAG, "Focus was requested on View ID " + viewID);
        View v = findViewById(viewID);
        v.requestFocus();
    }

    /**
     * Fill in the input fields with the existing Item's values if the activity is in edit mode
     */
    private void fillInputFields() {
        Item existingItem = getExistingItem(rowID);

        editItemName.setText(existingItem.getName());

        // TODO Set the spinner
        // editItemCategory.set()
        // editItemWeight.set()
        // editItemWeightUnits.set()
        // editItemQuantity.set()
        // editItemEssential.set()

        editItemNotes.setText(existingItem.getNotes());
    }



    /* Methods that respond to user clicks */

    /**
     * Respond when the user presses the 'Submit' button.
     * Create the new Item in the database if running in normal mode,
     * and update the Item in the database if running in edit mode.
     * @param v The View that was clicked
     */
    public void submit(View v) {
        // Read the input fields
        String itemName = editItemName.getText().toString();
        String itemCategory = editItemCategory.getSelectedItem().toString();
        int itemWeight = editItemWeight.getValue();
        String itemWeightUnits = editItemWeightUnits.getSelectedItem().toString();
        int itemQuantity = editItemQuantity.getValue();
        boolean itemIsEssential = editItemEssential.isChecked();
        String itemNotes = editItemNotes.getText().toString();
        String itemPhoto = "";

        // Construct a new Item object as read from the current input fields
        Item newItem = new Item(itemName,
                                itemCategory,
                                itemWeight,
                                itemWeightUnits,
                                itemQuantity,
                                itemIsEssential,
                                itemNotes,
                                itemPhoto);

        /* Normal mode: Add a new Item */
        if (!editMode) {
            // Log the new Item's toString
            Log.i(TAG, "Item created:\n" + newItem.toString());

            // Write the new Item to the Item database and get its row ID
            long rowID = addItemToDatabase(newItem);
            // The row ID was already logged in addItemToDatabase()

            // Close the database helper and finish the activity
            itemDbHelper.close();
            this.finish();
        }
        /* Edit mode: Update an existing Item */
        else {
            // Log the updated Item's toString
            Log.i(TAG, "Item updated:\n" + newItem.toString());

            // Update the Item that already exists in the database
            updateItemInDatabase(newItem); // TODO Check that updates complete ok, and that logging is solid

            // Close the database helper and finish the activity
            itemDbHelper.close();
            this.finish();
        }
    }

    /**
     * Clear all of the input fields in the activity's UI
     * @param v
     */
    public void clearFields(View v) {
        Utility.clearFields((ViewGroup) findViewById(R.id.viewgroup_add_item));
    }



    /* Methods that read from the database */

    /**
     * Given the row ID, return the Item that is currently stored in the database,
     * or null if no such Item exists
     * @param myRowID The row ID of the Item to fetch
     * @return The Item that has the row ID <code>rowID</code>, or null if no such Item exists
     */
    private Item getExistingItem(long myRowID) {
        // Access the database
        SQLiteDatabase db = itemDbHelper.getReadableDatabase();

        // Select by the row ID
        String[] selectionArg = { String.valueOf(myRowID) };

        // Query the database and get a cursor
        Cursor c = db.query(
                ItemContract.ItemEntry.TABLE_NAME,
                ItemDetailActivity.PROJECTION,
                ItemDetailActivity.ROW_ID_SELECTION,    // Select by the row ID.
                selectionArg,   // Pass the row ID as the selection argument.
                null,           // Do not group the rows in the result.
                null,           // Set to null because 'groupBy' is also set to null.
                null);          // Do not sort the result, only 1 row is expected.

        Log.i(TAG, c.getCount() + " row(s) in the cursor, 1 row expected");

        // Return null if a row was not found with the given row ID
        if (c.getCount() == 0) {
            itemDbHelper.close();
            Log.d(TAG, "No row found with row ID " + myRowID);
            return null;
        } else if (c.getCount() != 1) {
            Log.d(TAG, "Query for row ID " + myRowID + " was unsuccessful");
        }

        // Read the fields from the cursor
        c.moveToFirst();
        String existingItemName =
                c.getString(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_ITEM_NAME));
        String existingItemCategory =
                c.getString(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_CATEGORY));
        int existingItemWeight =
                c.getInt(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT));
        String existingItemWeightUnits =
                c.getString(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_UNITS));
        int existingItemQuantity =
                c.getInt(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_IN_GRAMS));
        boolean existingItemIsEssential =
                (c.getInt(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_IS_ESSENTIAL)) == 1);
        String existingItemNotes =
                (c.getString(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_NOTES)));
        String existingItemPhotoFilePath =
                (c.getString(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_PHOTO_FILE_PATH)));

        Item existingItem = new Item(existingItemName,
                existingItemCategory,
                existingItemWeight,
                existingItemWeightUnits,
                existingItemQuantity,
                existingItemIsEssential,
                existingItemNotes,
                existingItemPhotoFilePath);
        return existingItem;
    }

    /**
     * Given the row ID, return the name of the Item that is currently stored in the database,
     * or null if no such Item exists
     * @param myRowID The row ID of the Item to fetch
     * @return The name of the Item that has the row ID <code>rowID</code>,or null if no such Item exists
     */
    private static String getExistingItemName(long myRowID, SQLiteDatabase db) {
        // Select by the row ID
        String[] selectionArg = { String.valueOf(myRowID) };

        // Query the database and get a cursor
        Cursor c = db.query(
                ItemContract.ItemEntry.TABLE_NAME,
                ItemDetailActivity.PROJECTION,
                ItemDetailActivity.ROW_ID_SELECTION,    // Select by the row ID.
                selectionArg,   // Pass the row ID as the selection argument.
                null,           // Do not group the rows in the result.
                null,           // Set to null because 'groupBy' is also set to null.
                null);          // Do not sort the result, only 1 row is expected.

        Log.i(TAG, "Querying for the name of the Item with row ID " + myRowID + "...");
        Log.i(TAG, c.getCount() + " row(s) in the cursor, 1 row expected");

        // Return null if a row was not found with the given row ID
        if (c.getCount() == 0) {
            Log.d(TAG, "No row found with row ID " + myRowID);
            return null;
        } else if (c.getCount() != 1) {
            Log.d(TAG, "Query for row ID " + myRowID + " was unsuccessful");
            return null;
        }

        // Read the fields from the cursor
        c.moveToFirst();
        String existingItemName =
                c.getString(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_ITEM_NAME));
        Log.i(TAG, "The Item with row ID " + myRowID + " is named '" + existingItemName + "'");
        return existingItemName;
    }



    /* Methods that write to the database */

    /**
     * Add a completely new Item to the database
     * @return <code>long</code> row ID for the newly created Item's row ID in the database,
     * or <code>-1</code> if the database insert was not successful
     */
    private long addItemToDatabase(Item item) {
        // Get the database in write mode
        SQLiteDatabase db = itemDbHelper.getWritableDatabase();

        // Create a map of values, with column names as the keys and data attributes as the values
        ContentValues values = new ContentValues();
        values.put(ItemContract.ItemEntry.COLUMN_NAME_ITEM_NAME, item.getName());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_CATEGORY, item.getCategory());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT, item.getWeight());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_UNITS, item.getWeightUnits());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_IN_GRAMS, item.getWeightInGrams());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_QUANTITY, item.getQuantity());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_IS_ESSENTIAL, item.getIsEssential());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_NOTES, item.getNotes());
        values.put(ItemContract.ItemEntry.COLUMN_NAME_PHOTO_FILE_PATH, item.getPhotoFilePath());

        // Insert the new row
        long newRowId = db.insert(ItemContract.ItemEntry.TABLE_NAME, null, values);

        if (newRowId == -1) {
            /* The database insert was unsuccessful */
            Log.e(TAG, "New Item database insert was unsuccessful!");
        } else {
            /* The database insert was successful */
            History.log(new LogDbHelper(getApplicationContext()).getWritableDatabase(), "New item created: " + item.getName()); // XXX
            Log.i(TAG, "New Item row ID, " + newRowId);
        }
        return newRowId;
    }

    /**
     * Update an existing Item in the database
     * @param newItem The new Item to write to the database, overwriting the existing Item that has
     *                the row ID <code>rowID</code>
     * @return True if the update was successful, false if it was unsuccessful
     */
    private boolean updateItemInDatabase(Item newItem) {
        // Get the database in write mode
        SQLiteDatabase db = itemDbHelper.getWritableDatabase();

        // Get the Item that currently exists in the database so that it can be compared against
        // the new Item, as read from the input fields
        Item existingItem = getExistingItem(rowID);

        // Create a map of values, with column names as the keys and data attributes as the values
        ContentValues values = new ContentValues();
        if (!existingItem.getName().equals(newItem.getName())) {
            values.put(ItemContract.ItemEntry.COLUMN_NAME_ITEM_NAME, newItem.getName());
        } if (!existingItem.getCategory().equals(newItem.getCategory())) {
            values.put(ItemContract.ItemEntry.COLUMN_NAME_CATEGORY, newItem.getCategory());
        } if (existingItem.getWeight() != newItem.getWeight()) {
            values.put(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT, newItem.getWeight());
        } if (!existingItem.getWeightUnits().equals(newItem.getWeightUnits())) {
            values.put(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_UNITS, newItem.getWeightUnits());
        } if (existingItem.getWeightInGrams() != newItem.getWeightInGrams()) {
            values.put(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_IN_GRAMS, newItem.getWeightInGrams());
        } if (existingItem.getQuantity() != newItem.getQuantity()) {
            values.put(ItemContract.ItemEntry.COLUMN_NAME_QUANTITY, newItem.getQuantity());
        } if (existingItem.getIsEssential() != newItem.getIsEssential()) {
            values.put(ItemContract.ItemEntry.COLUMN_NAME_IS_ESSENTIAL, newItem.getIsEssential());
        } if (!existingItem.getNotes().equals(newItem.getNotes())) {
            values.put(ItemContract.ItemEntry.COLUMN_NAME_NOTES, newItem.getNotes());
        } if (!existingItem.getPhotoFilePath().equals(newItem.getPhotoFilePath())) {
            values.put(ItemContract.ItemEntry.COLUMN_NAME_PHOTO_FILE_PATH, newItem.getPhotoFilePath());
        }

        // Select by the row ID
        String[] selectionArg = { String.valueOf(rowID) };

        // Insert the new row
        int numRowsAffected = db.update(ItemContract.ItemEntry.TABLE_NAME,
                                        values,
                                        ItemDetailActivity.ROW_ID_SELECTION,
                                        selectionArg);

        if (numRowsAffected == 1) {
            /* The database update was successful */
            Log.i(TAG, "Item updated in database, row ID " + rowID);
            return true;
        } else if (numRowsAffected == 0) {
            /* The database update was unsuccessful */
            Log.e(TAG, "Item update for row ID " + rowID + " was unsuccessful!");
            return false;
        } else {
            // The number of rows affected was not zero or one,
            // which means that db.update() returned a number greater than one,
            // or a negative number. This is cause for concern!
            // Multiple rows may exist in the database that have the same row ID,
            // or db.update() returned a completely unexpected and unspecified result.
            Log.wtf(TAG, "Item update returned an unexpected result: " + numRowsAffected + "!");
            return false;
        }
    }



    /* Methods that manage the options menu */

    /**
     * Initialize the activity's options menu
     * @param menu The options menu in which to place items
     * @return True if the menu is to be displayed, false otherwise
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_item, menu);
        return true;
    }

    /**
     * Called whenever an item in the options menu is selected
     * @param item The menu item that was selected
     * @return Return false to allow normal menu processing to proceed, true to consume it here
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
