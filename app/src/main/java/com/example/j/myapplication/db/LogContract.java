package com.example.j.myapplication.db;

import android.provider.BaseColumns;

public final class LogContract {

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor
    public LogContract() {}

    public static abstract class LogEntry implements BaseColumns {
        // Inherited fields _ID and _COUNT
        public static final String TABLE_NAME = "log";
        public static final String COLUMN_NAME_CREATION_TIME = "created_at";
        public static final String COLUMN_NAME_MESSAGE = "message";
        public static final String COLUMN_NAME_PACK_A = "pack_A";
        public static final String COLUMN_NAME_PACK_B = "pack_B";
        public static final String COLUMN_NAME_ITEM_A = "item_A";
        public static final String COLUMN_NAME_ITEM_B = "item_B";
    }

}
