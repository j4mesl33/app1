package com.example.j.myapplication.db;

import android.provider.BaseColumns;

/**
 * @author James
 */
public final class ItemContract {

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor
    public ItemContract() {}

    /**
     *
     */
    public static abstract class ItemEntry implements BaseColumns {
        // Inherited fields _ID and _COUNT
        public static final String TABLE_NAME = "item";
        public static final String COLUMN_NAME_ITEM_NAME = "name";
        public static final String COLUMN_NAME_CATEGORY = "category";
        public static final String COLUMN_NAME_WEIGHT = "weight";
        public static final String COLUMN_NAME_WEIGHT_UNITS = "weight_units";
        public static final String COLUMN_NAME_WEIGHT_IN_GRAMS = "weight_in_grams";
        public static final String COLUMN_NAME_QUANTITY = "quantity";
        public static final String COLUMN_NAME_IS_ESSENTIAL = "is_essential";
        public static final String COLUMN_NAME_NOTES = "notes";
        public static final String COLUMN_NAME_PHOTO_FILE_PATH = "photo_file_path";
    }

}
