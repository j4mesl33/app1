package com.example.j.myapplication;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Pack {

    private static final int CUBIC_INCHES_PER_LITER = 61;

    private String name;
    private int capacity;
    private String capacityUnits;
    private int capacityInCubicInches;
    private String notes;
    private List<Integer> itemRowIDs;

    public Pack() {
        name = "";
        capacity = 0;
        capacityUnits = "";
        capacityInCubicInches = 0;
        notes = "";
        itemRowIDs = new ArrayList<Integer>();
    }

    public Pack(String name,
            int capacity,
            String capacityUnits,
            String notes) {
        this.name = name;
        this.capacity = capacity;
        this.capacityUnits = capacityUnits;
        this.capacityInCubicInches = getCapacityInCubicInches(capacity, capacityUnits);
        this.notes = notes;
        this.itemRowIDs = new ArrayList<Integer>();
    }

    private int getCapacityInCubicInches(int capacity, String capacityUnits) {
        if (capacityUnits.equals("liters")) {
            return capacity * CUBIC_INCHES_PER_LITER;
        } else if (capacityUnits.equals("cubic inches")) {
            return capacity;
        } else {
            throw new IllegalArgumentException("Unknown capacity units: " + capacityUnits);
        }
    }

    public int calculateTotalWeightInGrams(List<Integer> itemRowIDs) {
        return 0; // TODO
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public int getCapacity() { return capacity; }

    public void setCapacity(int capacity) { this.capacity = capacity; }

    public String getCapacityUnits() { return capacityUnits; }

    public void setCapacityUnits(String capacityUnits) { this.capacityUnits = capacityUnits; }

    public int getCapacityInCubicInches() { return capacityInCubicInches; }

    public void setCapacityInCubicInches(int capacityInCubicInches) { this.capacityInCubicInches = capacityInCubicInches; }

    public String getNotes() { return notes; }

    public void setNotes(String notes) { this.notes = notes; }

    public List<Integer> getItemRowIDs() {
        return Collections.unmodifiableList(itemRowIDs);
    }

    public void addItem(int rowID) { itemRowIDs.add(Integer.valueOf(rowID)); }

    /**
     * Remove the Item with row ID <code>rowID</code>
     * @param rowID The ID of the row to remove
     * @return True if the row ID was removed, false if it was nice
     */
    public boolean removeItem(int rowID) {
        boolean removed = itemRowIDs.remove(Integer.valueOf(rowID));
        return removed;
    }

    public void removeAllItems() { itemRowIDs = new ArrayList<Integer>(); }

    public int getNumItems() { return itemRowIDs.size(); }

    public boolean hasZeroItems() { return itemRowIDs.isEmpty(); }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Pack: " + name + "\n");
        sb.append("Capacity: " + capacity + " " + capacityUnits);
        sb.append(" (" + capacityInCubicInches + " cubic inches)\n");
        sb.append("Notes: " + notes + "\n");
        // sb.append("Total weight: " + calculateTotalWeightInGrams(itemRowIDs) + " grams\n"); // TODO

        if (hasZeroItems()) {
            sb.append("[Pack is currently empty]\n");
        } else {
            sb.append("Item row IDs:\n");
            for(Integer i : itemRowIDs) {
                sb.append(i + "\n");
            }
        }

        return sb.toString();
    }

}
