package com.example.j.myapplication;

import android.app.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.j.myapplication.db.PackContract;
import com.example.j.myapplication.db.PackDbHelper;

/*
  Completely organized:                         12/08/14
  Everything in this class last accounted for:  11/19/14

  TODO: Fix the back button behavior
  - If the user is in this activity and long-presses an item, they launch AddPackActivity
    to edit a field
  - If the user presses the 'Back' button in that activity, they return to ListPacksActivity
    instead of this activity, PackDetailActivity (because it is set as the parent in the XML)
  - So, fix this so that the 'Back' button returns to the activity that launched it

  TODO: Also need to refresh this activity's TextViews after editing them in AddPack (Edit mode)


 */


/**
  An activity that allows the user to view more detailed information about a Pack in the database

                                                                <br><br>
  Constants							                            {@link #TAG}<br>
  Member variables                                              {@link #rowID}<br>
  Constants and member variables used for photo functionality	TODO
  UI elements                                                   {@link layout}<br><br>

  Android activity lifecycle methods                            {@link #onCreate(Bundle)}<br>
  Initialization methods                                        {@link #getUiHooks()}<br>
  Methods that respond to user clicks                           {@link #onLongClick(View)}<br>
  Methods that manage the options menu                          {@link #onCreateOptionsMenu(Menu)}<br><br>

  @author James
 */
public class PackDetailActivity extends Activity implements View.OnLongClickListener {

    /* Constants */

    /**
     * Android logging tag
     */
    private static final String TAG = "PackDetailActivity";

    /**
     * Intent extra key used to pass the row ID of the Pack to display
     */
    public static final String ROW_ID_KEY = "RowID";

    /**
     * The columns to select from the database table
     */
    public static final String[] PROJECTION = {
            PackContract.PackEntry._ID,
            PackContract.PackEntry.COLUMN_NAME_PACK_NAME,
            PackContract.PackEntry.COLUMN_NAME_CAPACITY,
            PackContract.PackEntry.COLUMN_NAME_CAPACITY_UNITS,
            PackContract.PackEntry.COLUMN_NAME_CAPACITY_IN_CUBIC_INCHES,
            PackContract.PackEntry.COLUMN_NAME_NOTES };

    /**
     * The selection clause to select a row by its ID
     */
    public static final String ROW_ID_SELECTION = PackContract.PackEntry._ID + " = ?";



    /* Member variables */

    /**
     * The row ID of the Pack to display
     */
    private long rowID;

    /**
     * The database helper for the Pack database
     */
    private PackDbHelper packDbHelper;



    /* UI elements */

    /**
     * The view group that stores this activity's views
     */
    private LinearLayout layout;



    /* Android activity lifecycle methods */

    /**
     * Called when the activity is starting
     * @param savedInstanceState If the activity is being re-initialized after previously being shut
     *                           down, then this Bundle contains the data it most recently supplied
     *                           in onSaveInstanceState(Bundle). Otherwise it is null.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pack_detail);

        // Access the Packs database table
        packDbHelper = new PackDbHelper(getApplicationContext());

        // Get the row ID from the Intent extra
        Intent intent = getIntent();
        rowID = intent.getExtras().getLong(ROW_ID_KEY);

        // Find the necessary Views, and log error messages if any are not found
        getUiHooks();
        readIntoTextViews();

        Log.i(TAG, "Activity created");
        Log.i(TAG, "Row ID " + rowID + " received from ListPacksActivity");
    }



    /* Initialization methods */

    /**
     * Set references to the UI elements
     */
    private void getUiHooks() {
        // Find the view group used by the activity
        layout = (LinearLayout) findViewById(R.id.viewgroup_pack_detail);
    }

    /**
     * Read the Pack's information from the database and display it in the UI
     */
    private void readIntoTextViews() {
        // Get the UI hooks
        TextView textPackName = (TextView) findViewById(R.id.pack_name);
        TextView textPackCapacity = (TextView) findViewById(R.id.pack_capacity);
        TextView textPackCapacityCubicInches = (TextView) findViewById(R.id.pack_capacity_cubic_inches);
        TextView textPackNotes = (TextView) findViewById(R.id.pack_notes);

        // Log error messages if any necessary Views are not found
        if (textPackName == null) {
            Log.e(TAG, "TextView 'pack_name' not found!");
        } if (textPackCapacity == null) {
            Log.e(TAG, "TextView 'pack_capacity' not found!");
        } if (textPackCapacityCubicInches == null) {
            Log.e(TAG, "TextView 'pack_capacity_cubic_inches' not found!");
        } if (textPackNotes == null) {
            Log.e(TAG, "TextView 'pack_notes' not found!");
        }

        // Set the TextViews as long-clickable and set the listener (this class)
        textPackName.setLongClickable(true);
        textPackName.setOnLongClickListener(this);
        textPackCapacity.setLongClickable(true);
        textPackCapacity.setOnLongClickListener(this);
        textPackCapacityCubicInches.setLongClickable(true);
        textPackCapacityCubicInches.setOnLongClickListener(this);
        textPackNotes.setLongClickable(true);
        textPackNotes.setOnLongClickListener(this);

        // Get the database in read mode
        SQLiteDatabase db = packDbHelper.getReadableDatabase();

        // Select by the row ID
        String[] selectionArg = { String.valueOf(rowID) };

        // Query the database and get a cursor
        Cursor c = db.query(
                PackContract.PackEntry.TABLE_NAME, 
                PROJECTION, 
                ROW_ID_SELECTION,	// Select by the row ID.
                selectionArg,		// Pass the row ID as the selection argument.
                null,		    	// Do not group the rows in the result.
                null,		    	// Set to null because 'groupBy' is also set to null.
                null);

        Log.i(TAG, c.getColumnCount() + " columns in the cursor");

        // Read the fields from the cursor
        c.moveToFirst();
        String packName =
                c.getString(c.getColumnIndexOrThrow(PackContract.PackEntry.COLUMN_NAME_PACK_NAME));
        int packCapacity =
                c.getInt(c.getColumnIndexOrThrow(PackContract.PackEntry.COLUMN_NAME_CAPACITY));
        String packCapacityUnits =
                c.getString(c.getColumnIndexOrThrow(PackContract.PackEntry.COLUMN_NAME_CAPACITY_UNITS));
        int packCapacityCubicInches =
                c.getInt(c.getColumnIndexOrThrow(PackContract.PackEntry.COLUMN_NAME_CAPACITY_IN_CUBIC_INCHES));
        String packNotes =
                c.getString(c.getColumnIndexOrThrow(PackContract.PackEntry.COLUMN_NAME_NOTES));

        // Set the TextViews with the fields read from the Cursor
        textPackName.setText(packName);
        textPackCapacity.setText(packCapacity + " " + packCapacityUnits);
        textPackCapacityCubicInches.setText("(" + packCapacityCubicInches
                + " " + getResources().getString(R.string.cubic_inches) + ")");
        textPackNotes.setText(packNotes);

        // Set the activity's title to the Pack's name
        setActivityTitle(packName);
    }

    /**
     * Set the title of the activity in the action bar
     */
    private void setActivityTitle(String title) {
        getActionBar().setTitle(title);
    }



    /* Methods that respond to user clicks */

    /**
     * Called when a View was long-clicked
     * @param v The view that was long-clicked
     */
    @Override
    public boolean onLongClick(View v) {
        Log.i(TAG, "A view was long-clicked");
        int viewID = 0;

        // Open the Pack in edit mode, and request focus on a specific attribute
        if (v.getId() == R.id.pack_name) {
            Log.i(TAG, "Requesting focus on 'pack_name'");
            viewID = R.id.edit_pack_name;
        } else if (v.getId() == R.id.pack_capacity) {
            Log.i(TAG, "Requesting focus on 'pack_capacity'");
            viewID = R.id.edit_pack_capacity;
        } else if (v.getId() == R.id.pack_capacity_cubic_inches) {
            Log.i(TAG, "Requesting focus on 'pack_capacity'");
            viewID = R.id.edit_pack_capacity;
        } else if (v.getId() == R.id.pack_notes) {
            Log.i(TAG, "Requesting focus on 'pack_notes'");
            viewID = R.id.edit_pack_notes;
        }

        // Open AddPackActivity in Edit mode
        editPackAndRequestFocus(viewID);
        return true;
    }

    /**
     * Open AddPackActivity in order to edit the Pack, and request focus on the specified View
     * @param viewID The ID of the View on which to focus
     */
    private void editPackAndRequestFocus(int viewID) {
        Log.i(TAG, "Open AddPack to edit row ID " + rowID + " and focus on View ID " + viewID);
        Intent intent = new Intent(this, AddPackActivity.class);
        intent.putExtra(AddPackActivity.ROW_ID_KEY, rowID);
        intent.putExtra(AddPackActivity.VIEW_FOCUS_KEY, viewID);
        startActivity(intent);
    }



    /* Methods that manage the options menu */

    /**
     * Initialize the activity's option menu
     * @param menu The options menu in which to place items
     * @return True if the menu is to be displayed, false otherwise
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present
        getMenuInflater().inflate(R.menu.pack_detail, menu);
        return true;
    }

    /**
     * Called whenever an item in the options menu is selected
     * @param item The menu item that was selected
     * @return Return false to allow normal menu processing to proceed, true to consume it here
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
