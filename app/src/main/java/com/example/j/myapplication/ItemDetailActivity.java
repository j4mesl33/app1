package com.example.j.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.j.myapplication.db.ItemContract;
import com.example.j.myapplication.db.ItemDbHelper;

import java.io.File;
import java.io.IOException;
import java.net.URI;

/*
 
  Camera and photo functionality moved from AddItem:    12/07/14
  Methods organized and completely documented:          12/07/14
  Everything in this class last accounted for:          11/20/14

  TODO:
  - This class must correctly name the saved photo file, using the row ID

  - Add an "Edit" option to the options menu

  - Must also make changes to manage the flow between the activities from the user's viewpoint

 */

/**

  An activity that allows the user to view more detailed information about an Item in the database

                                                                        <br><br>
    Constants                                                           {@link #TAG}<br>
    Member variables                                                    {@link #rowID}<br>
    Constants and member variables used for photo functionality         {@link #PHOTO_DIRECTORY}<br>
    UI elements                                                         {@link #textItemName}<br><br>

    Android lifecycle methods (including photo functionality)           {@link #onCreate(Bundle)}<br>
    Initialization methods                                              {@link #getUiHooks()}<br>
    Methods that respond to user clicks                                 {@link #onLongClick(View)}<br>
    Methods that manage the options menu                                {@link #onCreateOptionsMenu(Menu)}<br>
    Other methods                                                       {@link #showPhotoIcon()}<br><br>

  @author James
 */
public class ItemDetailActivity extends Activity
        implements View.OnLongClickListener { /** To listen for long-clicks on the TextViews */

    /* Constants */

    /**
     * Android logging tag
     */
    private static final String TAG = "ItemDetailActivity";

    /**
     * Intent extra key to pass the row ID of the Item to display
     */
    public static final String ROW_ID_KEY = "RowID";

    /**
     * The columns to select from the database table
     */
    public static final String[] PROJECTION = {
            ItemContract.ItemEntry._ID,
            ItemContract.ItemEntry.COLUMN_NAME_ITEM_NAME,
            ItemContract.ItemEntry.COLUMN_NAME_CATEGORY,
            ItemContract.ItemEntry.COLUMN_NAME_WEIGHT,
            ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_UNITS,
            ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_IN_GRAMS,
            ItemContract.ItemEntry.COLUMN_NAME_QUANTITY,
            ItemContract.ItemEntry.COLUMN_NAME_IS_ESSENTIAL,
            ItemContract.ItemEntry.COLUMN_NAME_NOTES,
            ItemContract.ItemEntry.COLUMN_NAME_PHOTO_FILE_PATH };

    /**
     * The selection clause to select a row by its ID
     */
    public static final String ROW_ID_SELECTION = ItemContract.ItemEntry._ID + " = ?";



    /* Member variables */

    /**
     * The row ID of the Item to display
     */
    private long rowID;

    /**
     * The database helper for the Item database
     */
    private ItemDbHelper itemDbHelper;



    /* Constants and member variables relevant used for photo functionality */

    /**
     * The name of the directory in which photos will be stored
     */
    private static final String PHOTO_DIRECTORY = "/Item_Photos/";

    /**
     * The base filename of each photo in the photo directory, i.e. "Item_1", "Item_2"...
     */
    private static final String PHOTO_FILENAME_BASE = "Item_";

    /**
     * The file extension for each photo in the photo directory
     */
    private static final String PHOTO_FILE_EXTENSION = ".jpg";

    /**
     * The request code used to request a photo, sent with <code>Activity.startActivityForResult()</code>
     */
    private static final int TAKE_PHOTO_KEY = 0;

    /**
     * True if the photo icon is currently set, false otherwise
     */
    private boolean isPhotoSet;

    /**
     * The name of the directory in which photos are stored
     */
    private String newDirectoryName;

    /**
     * The URI of the new photo file
     */
    private static Uri newPhotoUri;



    /* UI elements */

    /**
     * Name
     */
    private TextView textItemName;

    /**
     * Category
     */
    private TextView textItemCategory;

    /**
     * Weight
     */
    private TextView textItemWeight;

    /**
     * Weight in grams
     */
    private TextView textItemWeightGrams;

    /**
     * Quantity
     */
    private TextView textItemQuantity;

    /**
     * "Is essential"
     */
    private TextView textItemIsEssential;

    /**
     * Notes
     */
    private TextView textItemNotes;

    /**
     * Photo
     */
    private ImageView itemPhoto;

    /**
     * The view group that stores this activity's views
     */
    private LinearLayout layout;



    /* Android activity lifecycle methods */

    /**
     * Called when the activity is starting
     * @param savedInstanceState If the activity is being re-initialized after previously being shut
     *                           down, then this Bundle contains the data it most recently supplied
     *                           in onSaveInstanceState(Bundle). Otherwise it is null.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);

        // Access the Items database table
        itemDbHelper = new ItemDbHelper(getApplication());

        // Get the row ID from the Intent extra
        Intent intent = getIntent();
        rowID = intent.getExtras().getLong(ROW_ID_KEY);

        Log.i(TAG, "Activity created");
        Log.i(TAG, "Row ID " + rowID + " received from ListItemsActivity");

        // Find the necessary Views, and log error messages if any are not found
        getUiHooks();

        // Fill the Views with information about the Item as read from the database
        readIntoTextViews();
    }

    /**
     * Receive a called activity's result
     * @param requestCode The request code
     * @param resultCode The result code
     * @param data The Intent passed from the called activity
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TAKE_PHOTO_KEY) {
            if (resultCode == RESULT_OK) {
                /* A photo was captured */

                // TODO Is anything returned in 'data'? Or is the new picture simply found at the URI?
                // TODO Save and display both the icon and the full-size photo

                Log.i(TAG, "Activity returned and successfully captured a photo");
                Log.i(TAG, "New photo URI (should be created): " + newPhotoUri.toString());
                showPhotoIcon();
            } else if (resultCode == RESULT_CANCELED) {
                /* A photo was not captured */
                Log.i(TAG, "Activity returned without capturing a photo");
            }
        }
    }



    /* Initialization methods */

    /**
     * Set references to the UI elements, and log error messages if any necessary Views
     * are not found
     */
    private void getUiHooks() {
        // Find the view group used by the activity
        layout = (LinearLayout) findViewById(R.id.viewgroup_item_detail);

        // Get the UI hooks
        textItemName = (TextView) findViewById(R.id.item_name);
        textItemCategory = (TextView) findViewById(R.id.item_category);
        textItemWeight = (TextView) findViewById(R.id.item_weight);
        textItemWeightGrams = (TextView) findViewById(R.id.item_weight_grams);
        textItemQuantity = (TextView) findViewById(R.id.item_quantity);
        textItemIsEssential = (TextView) findViewById(R.id.item_is_essential);
        textItemNotes = (TextView) findViewById(R.id.item_notes);
        itemPhoto = (ImageView) findViewById(R.id.item_photo);

        // Set the TextViews as long-clickable
        textItemName.setLongClickable(true);
        textItemCategory.setLongClickable(true);
        textItemWeight.setLongClickable(true);
        textItemWeightGrams.setLongClickable(true);
        textItemQuantity.setLongClickable(true);
        textItemIsEssential.setLongClickable(true);
        textItemNotes.setLongClickable(true);

        // Log error messages if any necessary Views are not found
        if (textItemName == null) { Log.e(TAG, "TextView 'item_name' not found!"); }
        if (textItemCategory == null) { Log.e(TAG, "TextView 'item_category' not found!"); }
        if (textItemWeight == null) { Log.e(TAG, "TextView 'item_weight' not found!"); }
        if (textItemWeightGrams == null) { Log.e(TAG, "TextView 'item_weight_grams' not found!"); }
        if (textItemQuantity == null) { Log.e(TAG, "TextView 'item_quantity' not found!"); }
        if (textItemIsEssential == null) { Log.e(TAG, "TextView 'item_is_essential' not found!"); }
        if (textItemNotes == null) { Log.e(TAG, "TextView 'item_notes' not found!"); }
        if (itemPhoto == null) { Log.e(TAG, "ImageView 'item_photo' not found!"); }

        // TODO XXX
        // Create a directory in which photos can be stored
        newDirectoryName = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                + PHOTO_DIRECTORY;
        File newDirectory = new File(newDirectoryName);
        boolean isDirectoryCreated = newDirectory.mkdirs();

        if (isDirectoryCreated) {
            Log.i(TAG, "New directory was created, " + newDirectory.getName());
        } else if (newDirectory.isDirectory()) {
            Log.i(TAG, "Directory was already found, " + newDirectory.getName());
        } else {
            Log.i(TAG, "Failed to create a new directory");
        }

    }

    /**
     * Read the Item's information from the database and display it in the UI
     */
    private void readIntoTextViews() {
        // Get the database in read mode
        SQLiteDatabase db = itemDbHelper.getReadableDatabase();

        // Select by the row ID
        String[] selectionArg = { String.valueOf(rowID) };

        // Query the database and get a cursor
        Cursor c = db.query(
                ItemContract.ItemEntry.TABLE_NAME,
                PROJECTION,
                ROW_ID_SELECTION,   // Select by the row ID.
                selectionArg,       // Pass the row ID as the selection argument.
                null,               // Do not group the rows in the result.
                null,               // Set to null because 'groupBy' is also set to null.
                null);

        Log.i(TAG, c.getColumnCount() + " columns in the cursor");

        // Read the fields from the Cursor
        c.moveToFirst();
        String itemName =
                c.getString(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_ITEM_NAME));
        String itemCategory =
                c.getString(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_CATEGORY));
        int itemWeight =
                c.getInt(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT));
        String itemWeightUnits =
                c.getString(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_UNITS));
        int itemWeightGrams =
                c.getInt(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_WEIGHT_IN_GRAMS));
        int itemQuantity =
                c.getInt(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_QUANTITY));
        boolean itemIsEssential =
                (c.getInt(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_IS_ESSENTIAL)) == 1);
        String itemNotes =
                c.getString(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_NOTES));
        String itemPhotoFilePath =
                c.getString(c.getColumnIndexOrThrow(ItemContract.ItemEntry.COLUMN_NAME_PHOTO_FILE_PATH));
        Log.i(TAG, "PHOTO LOCATION FROM DB: " + itemPhotoFilePath);

        // Set the TextViews with the fields read from the Cursor
        textItemName.setText(itemName);
        textItemCategory.setText(itemCategory);
        textItemWeight.setText(String.valueOf(itemWeight) + " " + itemWeightUnits);
        textItemWeightGrams.setText(" (" + String.valueOf(itemWeightGrams) + " grams)");
        textItemQuantity.setText(String.valueOf(itemQuantity));
        textItemNotes.setText(itemNotes);
        if (itemIsEssential) {
            textItemIsEssential.setText("An essential item");
        } else {
            textItemIsEssential.setText("Not an essential item");
        }

        // Set the Item image if the String file path is non-empty
        setImage(itemPhotoFilePath);

        // Set the activity's title to the Pack's name
        setActivityTitle(itemName);
    }

    /**
     * Set the image if the photo file path is non-empty. If it is empty, do nothing.
     * @param photoFilePath The photo file path where an image should be found
     */
    private void setImage(String photoFilePath) {
        /* The Item photo file path is non-empty */
        if (!photoFilePath.equals("")) {
            // Try creating the file, using the java.net.URI
            URI photoJavaUri = URI.create(photoFilePath);
            File photoFile = new File(photoJavaUri);

            if (!photoFile.exists()) {
                /* The file does not exist */
                Log.e(TAG, "The photo does not exist for Item " + rowID + ", file " + photoFilePath);

            } else if (!photoFile.canRead()) {
                /* The file cannot be read */
                Log.e(TAG, "The photo cannot be read for Item " + rowID + ", file " + photoFilePath);

            } else {
                /* The file exists and can be read */

                // Set the image using the android.net.Uri, and make it visible

                // A File can only be created using a java.net.URI,
                // and an ImageView can only be set using an android.net.Uri,
                // which is why two different types of URIs are used

                Uri photoUri = Uri.parse(photoFilePath);
                itemPhoto.setImageURI(photoUri);
                itemPhoto.setVisibility(View.VISIBLE);

                Log.i(TAG, "The photo for Item " + rowID + " was set");
                Log.i(TAG, "Photo file path: " + photoFilePath);
                Log.i(TAG, "Photo URI: " + photoUri);
            }
        }
        /* The Item photo file path is empty */
        else {
            Log.i(TAG, "No photo stored for Item " + rowID);
        }

    }

    /**
     * Set the title of the activity in the action bar
     */
    private void setActivityTitle(String title) {
        getActionBar().setTitle(title);
    }



    /* Methods that respond to user clicks */

    /**
     * Called when a View was long-clicked
     * @param v The view that was long-clicked
     */
    @Override
    public boolean onLongClick(View v) {
        Log.i(TAG, "A view was long-clicked");
        int viewID;

        if (v.getId() == R.id.item_name) {
            Log.i(TAG, "Requesting focus on 'item_name'");
            viewID = R.id.edit_item_name;
        } else if (v.getId() == R.id.item_category) {
            Log.i(TAG, "Requesting focus on 'item_category'");
            viewID = R.id.edit_item_category;
        } else if (v.getId() == R.id.item_weight) {
            Log.i(TAG, "Requesting focus on 'item_weight'");
             viewID = R.id.edit_item_weight;
        } else if (v.getId() == R.id.item_weight_grams) {
            Log.i(TAG, "Requesting focus on 'item_weight_grams'");
            viewID = R.id.edit_item_weight;
        } else if (v.getId() == R.id.item_quantity) {
            Log.i(TAG, "Requesting focus on 'item_quantity'");
            viewID = R.id.edit_item_quantity;
        } else if (v.getId() == R.id.item_is_essential) {
            Log.i(TAG, "Requesting focus on 'item_is_essential'");
            viewID = R.id.edit_item_essential;
        } else if (v.getId() == R.id.item_notes) {
            Log.i(TAG, "Requesting focus on 'item_notes'");
            viewID = R.id.edit_item_notes;
        } else {
            Log.e(TAG, "An unknown view was long-clicked");
            return true;
        }

        editPackAndRequestFocus(viewID);
        return true;
    }

    /**
     * Open AddItemActivity in order to edit the Item, and request focus on the specified View
     * @param viewID The ID of the View on which to focus
     */
    private void editPackAndRequestFocus(int viewID) {
        Log.i(TAG, "Open AddItem to edit row ID " + rowID + " and focus on View ID " + viewID);
        Intent intent = new Intent(this, AddItemActivity.class);
        intent.putExtra(AddItemActivity.ROW_ID_KEY, rowID);
        intent.putExtra(AddItemActivity.VIEW_FOCUS_KEY, viewID);
        startActivity(intent);
    }

    /**
     * Respond to a user's click on the "Take photo" button
     * @param v The View that was clicked
     */
    public void onClickTakePhoto(View v) {
        /* If the photo has already been set, clear it from the display */
        if (isPhotoSet) {
            hidePhotoIcon();
        }
        /* If the photo has not been set, allow the user to take a photo, and set it to be displayed */
        else {
            // Choose a filename, and create a file in which the photo can be stored
            String newPhotoName = newDirectoryName
                    + PHOTO_FILENAME_BASE
                    + rowID
                    + PHOTO_FILE_EXTENSION;
            File newPhoto = new File(newPhotoName);
            // TODO Try File.createTempFile()?
            // https://developer.android.com/training/camera/photobasics.html

            try {
                newPhoto.createNewFile();
            } catch (IOException e) {
                Utility.toast(getApplicationContext(), "Can't create a photo right now!");
                Log.e(TAG, "New file could not be created!");
                e.printStackTrace();
                return;
            }
            Log.i(TAG, "New file was created, " + newPhoto.getName());

            // Get the file URI
            newPhotoUri = Uri.fromFile(newPhoto);
            Log.i(TAG, "New photo URI (not yet created): " + newPhotoUri.toString());

            // Start an activity to capture an image, and pass it the photo file URI
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, newPhotoUri);
            startActivityForResult(cameraIntent, TAKE_PHOTO_KEY);
        }
    }



    /* Methods that manage the options menu */

    /**
     * Initialize the activity's option menu
     * @param menu The options menu in which to place items
     * @return True if the menu is to be displayed, false otherwise
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.item_detail, menu);
        return true;
    }

    /**
     * Called whenever an item in the options menu is selected
     * @param item The menu item that was selected
     * @return Return false to allow normal menu processing to proceed, true to consume it here
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    /* Other methods */

    /**
     * Show the icon that displays the Item's photo
     */
    private void showPhotoIcon() {
        isPhotoSet = true;

        // Find the Views
        Button photoButton = (Button) findViewById(R.id.button_take_photo_item);
        ImageView photo = (ImageView) findViewById(R.id.item_photo);
        TextView photoCaption = (TextView) findViewById(R.id.item_photo_caption);

        // Set the button to read "Delete photo", since a photo has just been set
        // TODO This must be changed when multiple photos are allowed (Use a DB field 'num photos'?)
        photoButton.setText(R.string.delete_photo);

        // Set the image, and set it to be visible
        photo.setImageURI(newPhotoUri);
        photo.setVisibility(View.VISIBLE);

        // Set the caption to display the URI
        photoCaption.setText(newPhotoUri.toString());
        photoCaption.setVisibility(View.VISIBLE);

        Log.i(TAG, "Photo icon was set");
    }

    /*
     * Hide the icon that displays the Item's photo
     */
    private void hidePhotoIcon() {
        isPhotoSet = false;

        // Find the Views
        Button photoButton = (Button) findViewById(R.id.button_take_photo_item);
        ImageView photo = (ImageView) findViewById(R.id.item_photo);
        TextView photoCaption = (TextView) findViewById(R.id.item_photo_caption);

        // Set the button to read "Take a photo", since the photo has just been removed
        photoButton.setText(R.string.take_photo_item);

        // Set the image to be blank and hidden
        photo.setImageURI(null);
        photo.setVisibility(View.GONE);

        // Set the caption to be blank and hidden
        photoCaption.setText("");
        photoCaption.setVisibility(View.GONE);

        // TODO Update the photo file path in the database to "" (i.e. Delete it from the database)
        // Other DB columns will be involved if multiple item photos are allowed

        Log.i(TAG, "Photo icon was removed");
    }
}
